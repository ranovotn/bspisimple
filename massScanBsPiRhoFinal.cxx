#include "BFitCore/IncludeAllSources.h"

#include "RooStats/AsymptoticCalculator.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodTestStat.h"

using namespace RooStats;
using namespace RooFit;
using namespace std;

const Double_t INF = RooNumber::infinity();

const Double_t signalBandBsMin = 5346.; // MeV
const Double_t signalBandBsMax = 5386.; // MeV
const TString leftSideBandBsCut = "BsMass < 5210"; // MeV
const TString rightSideBandBsCut = "BsMass > 5510"; // MeV

const Double_t PDGBsMass = 5366.77; // MeV
const Double_t PDGpiMass = 139.57; // MeV

const Double_t D0mean = 5567.8;
const Double_t D0gamma = 21.9;
const Double_t D0meanErrStat = 2.9;
const Double_t D0gammaErrStat = 6.4;
const Double_t D0meanErrSystUp = 0.9;
const Double_t D0meanErrSystDown = 1.9;
const Double_t D0gammaErrSystUp = 5.0;
const Double_t D0gammaErrSystDown = 2.5;


//void massScanBsPiRun1(const TString &name = "results/BsPi2011and2012RD_massScan", const Double_t &BsPtInt = 10., const Double_t &mean = 5567.8, const Double_t &systSF = 0., const Bool_t &fitPunzi = false) {
void massScanBsPiRun1(const TString &name = "results/BsPi2011and2012RD_massScan", const Double_t &BsPtInt = 10., const Double_t &mean = 5567.8, const TString typeBW = "Swave", const TString bckType = "rooAtlas", const Double_t &systSF = 0., const Bool_t &fitPunzi = false, Bool_t usePCE = false, Bool_t useSpurious = false, Bool_t barrel = true, Bool_t endcap = true, Int_t massSyst = 0, Int_t gammaSyst = 0, Bool_t useBinnedEpsRel=true, TString year = "2012" ) {
    const time_t startTime = TTimeStamp().GetSec();
    SetAtlasStyle();
    checkDir(name);
    TString suffix = Form("_BpTgt%.0fGeV_mean%.2fMeV_systSF%.0f", BsPtInt, mean, systSF);

    const Double_t SF      = 1.07;
    const Double_t sigmaSF = 0.04;
    const TString cut      = Form("B_pT > %.0f && BsPiSubMass < 5900 && had_3pT > 1000 && had_4pT > 1000 && had_5pT > 500 && BsTau > 0.2 && Track5FromPV && bestChiBs && BsMass > %.0f && BsMass < %.0f", BsPtInt * 1000., signalBandBsMin, signalBandBsMax); 
    suffix += "_defaultCuts";


    cout <<"INFO: FitModel" <<endl;
    cout <<"\t\t Bck Type: "<<bckType <<endl;
    cout <<"\t\t Sig Type: "<<typeBW<<endl;
    cout <<"\t\t usePCE: "<<usePCE <<endl;
    cout <<"\t\t useSpurious: "<<useSpurious <<endl;
    cout <<"\t\t barrel: "<<barrel <<endl;
    cout <<"\t\t endcap: "<<endcap <<endl;
    cout <<"\t\t massSyst: "<<massSyst <<endl;
    cout <<"\t\t gammaSyst: "<<gammaSyst <<endl;
    cout <<"\t\t useBinnedEpsRel: "<<useBinnedEpsRel <<endl;

    suffix += "_"+typeBW;

    

    Observables *observables = new Observables("observables");
    observables->set(MASS, "BsPiSubMass",  "#font[52]{m}(#font[52]{B_{s}}^{0}#font[152]{#pi}^{#pm})", PDGBsMass+PDGpiMass, PDGBsMass+PDGpiMass + 300.);
    observables->set(MASSERROR, "RefittedBsPimassErr", "#font[152]{#sigma}_{#font[52]{m}}(#font[52]{B_{s}}^{0}#font[152]{#pi}^{#pm})", .001, .001 + 30.);
    RooRealVar *BsPt = new RooRealVar("B_pT", "#font[52]{B_{s}}^{0} #font[52]{p}_{T}", 0., 15e4, "MeV"); // for epsRel
    observables->add(BsPt); // for epsRel

    Data *data11 = new Data("data11", "data11cascadeBsPi.bsConstraint.NoBestChi.v10.root", "BsPiCascade CascadeExtraInfo", observables, cut);
    Data *data12 = new Data("data12", "data12cascadeBsPi.bsConstraint.NoBestChi.v10.root", "BsPiCascade CascadeExtraInfo", observables, cut);

    Data *data;
    if(year == "2012") data = data12;
    else if(year == "2011") data = data11;
    else if(year == "2011and2012"){
        data = data12;
        data->add(data11);
    }
    else {
        cout << "ERROR: Invalid input year!"<< endl;
        return;
    }
    data->SetName("data");

    // Mass window
    // ====================================
    // RelMassError function
    Double_t r0 = -7.76044e+3;
    Double_t r1 =  5.41293e+3;
    Double_t r2 =  1.40965e-1;
    Double_t r3 =  3.16440e-2;
    Double_t r4 = -1.78405e-6;
    Double_t calcMassErr = ( mean - 5366.6 ) * ( r0 * TMath::Landau( mean, r1, r2 ) + ROOT::Math::Chebyshev1( mean, r3, r4 ) );

    // BW(mean, gamma_D0) (x) gauss(0., calcMassErr); signal PDF shape only, no fitting
    ModelPDF *massWindowModel = new ModelPDF("massWindow", data);
    massWindowModel->setMassSignalPDF("massWindowSig", "relBW"+typeBW);
    massWindowModel->setMassResolutionPDF("massWindowRes", "gauss");
    
    /*massWindowModel->setPars("massWindowSig_mean", mean, true); // D0
    massWindowModel->setPars("massWindowSig_gamma", D0gamma, true); // D0
    */
    massWindowModel->setPars("massWindowSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
    massWindowModel->setPars("massWindowSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0    
    massWindowModel->setPars("massWindowSig_massA", PDGBsMass, true);
    massWindowModel->setPars("massWindowSig_massB", PDGpiMass, true);
    massWindowModel->setPars("massWindowRes_mean", 0., true);
    massWindowModel->setPars("massWindowRes_sigma", ( SF + ( systSF * sigmaSF ) ) * calcMassErr, true);

    // BsPi mass range - signal = PDF_value > 0.05 max_PDF_value; max_PDF_value != PDF_value(@mean), I don't know why...
    Int_t nSteps = 3000;
    Double_t maxPdfVal = 0.;
    Double_t pdfVal = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        observables->get(MASS)->setVal( 5500. + (Double_t)i / 10.0 );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        maxPdfVal = ( pdfVal > maxPdfVal ? pdfVal : maxPdfVal );
    }
    Double_t min = 0.;
    Double_t max = 0.;
    Double_t val = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        val = 5500. + (Double_t)i / 10.0;
        observables->get(MASS)->setVal( val );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        if ( min == 0. ) {
            if ( pdfVal / maxPdfVal >= 0.05 ) min = val;
        } else if ( max == 0. ) {
            if ( pdfVal / maxPdfVal <= 0.05 ) max = val;
        }
    }
    if ( min < observables->get(MASS)->getMin() || max > observables->get(MASS)->getMax() || min == 0. || max == 0. ) {
        cout << "INFO: Mass window ( " << min << " ; " << max << " ) MeV is out of the data range ( " << observables->get(MASS)->getMin() << " ; " << observables->get(MASS)->getMax() << " ) MeV." << endl;
        return;
    }
    // calculate "corr500 * 1/corrPion" for each Bs_pT in this (BsPi mass) range, make an average and inverse value ( = epsRel)
    TString reduceCut = "BsPiSubMass > ";
    reduceCut += min;
    reduceCut += " && BsPiSubMass < ";
    reduceCut += max;
    Data *dataReduced = data->reduce("dataReduced", reduceCut);

    Long64_t nReduced = dataReduced->getDataset()->numEntries();

    Double_t p0[3];
    Double_t p1[3];
    Double_t p2[3];
    Double_t p3[3];
    Double_t p4[3];
    Double_t q0[3];
    Double_t q1[3];
    Double_t q2[3];
    Double_t q3[3];

    if (barrel && endcap){
        if(useBinnedEpsRel){
            p0[0] =  5.31903e+0;
            p1[0] = -4.08609e-4;
            p2[0] =  2.48093e+0;
            p3[0] = -6.89892e-5;
            p4[0] =  3.98142e-10;
            q0[0] =  2.69890e+0;
            q1[0] = -2.59078e-4;
            q2[0] =  1.32936e+0;
            q3[0] = -6.60074e-6;

            p0[1] =  3.43427e+0;
            p1[1] = -3.16100e-4;
            p2[1] =  2.03849e+0;
            p3[1] = -4.30101e-5;
            p4[1] =  2.24453e-10;
            q0[1] =  1.57724e+0;
            q1[1] = -3.01711e-4;
            q2[1] =  1.44780e+0;
            q3[1] = -6.87981e-6;
                
            p0[2] =  2.50898e+0;
            p1[2] = -2.67756e-4;
            p2[2] =  1.75799e+0;
            p3[2] = -2.77793e-5;
            p4[2] =  1.32081e-10;
            q0[2] = -8.43088e+0;
            q1[2] = -7.13354e-4;
            q2[2] =  1.37236e+0;
            q3[2] = -2.73718e-6;
        }
        else{
            p0[0] = p0[1] = p0[2] =  3.88384e+0;
            p1[0] = p1[1] = p1[2] = -3.52755e-4;
            p2[0] = p2[1] = p2[2] =  2.12907e+0;
            p3[0] = p3[1] = p3[2] = -4.80883e-5;
            p4[0] = p4[1] = p4[2] =  2.58203e-10;
            q0[0] = q0[1] = q0[2] =  9.76032e-1;
            q1[0] = q1[1] = q1[2] = -2.04753e-4;
            q2[0] = q2[1] = q2[2] =  1.35443e+0;
            q3[0] = q3[1] = q3[2] = -4.38256e-6;
        }
    }
    else if(barrel){ 
        if(useBinnedEpsRel){
            p0[0] =  4.93495e+0;
            p1[0] = -3.81853e-4;
            p2[0] =  2.30788e+0;
            p3[0] = -5.93104e-5;
            p4[0] =  3.33016e-10;
            q0[0] =  2.11580e+0;
            q1[0] = -2.09079e-4;
            q2[0] =  1.18319e+0;
            q3[0] = -3.95755e-6;
                    
            p0[1] =  3.24873e+0;
            p1[1] = -3.03512e-4;
            p2[1] =  1.93239e+0;
            p3[1] = -3.81541e-5;
            p4[1] =  1.97590e-10;
            q0[1] =  2.19535e+0;
            q1[1] = -3.41660e-4;
            q2[1] =  1.36806e+0;
            q3[1] = -6.34577e-6;
            
            p0[2] =  1.88642e+0;
            p1[2] = -2.12971e-4;
            p2[2] =  1.50657e+0;
            p3[2] = -1.43228e-5;
            p4[2] =  4.47887e-11;
            q0[2] =  -8.61924e+0;
            q1[2] = -6.38711e-4;
            q2[2] =  1.32552e+0;
            q3[2] = -3.07364e-6;
        }
        else{
            p0[0] = p0[1] = p0[2] =  3.71273e+0;
            p1[0] = p1[1] = p1[2] = -3.46605e-4;
            p2[0] = p2[1] = p2[2] =  2.05107e+0;
            p3[0] = p3[1] = p3[2] = -4.41165e-5;
            p4[0] = p4[1] = p4[2] =  2.33609e-10;
            q0[0] = q0[1] = q0[2] =  1.00542e+0;
            q1[0] = q1[1] = q1[2] = -2.05837e-4;
            q2[0] = q2[1] = q2[2] =  1.28288e+0;
            q3[0] = q3[1] = q3[2] = -3.86472e-6;
        }
    }
    else if(endcap){ 
        if(useBinnedEpsRel){
            p0[0] =  5.63178e+0;
            p1[0] = -4.32112e-4;
            p2[0] =  2.62223e+0;
            p3[0] = -7.71731e-5;
            p4[0] =  4.54671e-10;
            q0[0] =  3.46251e+0;
            q1[0] = -3.19069e-4;
            q2[0] =  1.47281e+0;
            q3[0] = -9.44006e-6;
            
            p0[1] =  3.54207e+0;
            p1[1] = -3.22886e-4;
            p2[1] =  2.10342e+0;
            p3[1] = -4.59175e-5;
            p4[1] =  2.39939e-10;
            q0[1] =  2.19535e+0;
            q1[1] = -3.41660e-4;
            q2[1] =  1.36806e+0;
            q3[1] = -6.34577e-6;
    
            p0[2] =  3.06070e+0;
            p1[2] = -3.16608e-4;
            p2[2] =  1.95728e+0;
            p3[2] = -3.93035e-5;
            p4[2] =  2.11573e-10;
            q0[2] =  -1.59393e+0;
            q1[2] = -6.13077e-5;
            q2[2] =  1.32146e+0;
            q3[2] = -2.61075e-7;
            }
            else{
                p0[0] = p0[1] = p0[2] =  4.00544e+0;
                p1[0] = p1[1] = p1[2] = -3.57902e-4;
                p2[0] = p2[1] = p2[2] =  2.18582e+0;
                p3[0] = p3[1] = p3[2] = -5.10119e-5;
                p4[0] = p4[1] = p4[2] =  2.76416e-10;
                q0[0] = q0[1] = q0[2] =  1.20435e+0;
                q1[0] = q1[1] = q1[2] = -2.17018e-4;
                q2[0] = q2[1] = q2[2] =  1.43371e+0;
                q3[0] = q3[1] = q3[2] = -4.96948e-6;
        }
    }
    else{ 
        cout << "ERROR:No barrel or endcap selected." << endl;
        return; 
    }

    Double_t epsRel = 0.;
    for ( Long64_t j = 0; j < nReduced; j++ ) {
        Double_t x = dataReduced->getDataset()->get( j )->getRealValue("B_pT", -999.);
        Double_t BsPi_mass = dataReduced->getDataset()->get( j )->getRealValue("BsPiSubMass", -999.);
        if ( x == -999. ) {
            cout << "ERROR: Can't read Bs pT!" << endl;
            return;
        }
        if ( BsPi_mass == -999. ) {
            cout << "ERROR: Can't read BsPi mass!" << endl;
            return;
        }
        Double_t corr500 = 0;
        Double_t invEff = 0;

        if(BsPi_mass<5560){
            corr500 = exp( p0[0] + p1[0] * x ) + ROOT::Math::Chebyshev2( x, p2[0], p3[0], p4[0] );
            invEff = TMath::Exp( q0[0] + q1[0] * x ) + ROOT::Math::Chebyshev1( x, q2[0], q3[0] );
        }
        else if(BsPi_mass<5575){
            corr500 = exp( p0[1] + p1[1] * x ) + ROOT::Math::Chebyshev2( x, p2[1], p3[1], p4[1] );
            invEff = TMath::Exp( q0[1] + q1[1] * x ) + ROOT::Math::Chebyshev1( x, q2[1], q3[1] );
        }
        else{
            corr500 = exp( p0[2] + p1[2] * x ) + ROOT::Math::Chebyshev2( x, p2[2], p3[2], p4[2] );
            invEff = TMath::Exp( q0[2] + q1[2] * x ) + ROOT::Math::Chebyshev1( x, q2[2], q3[2] );
        }

        epsRel += ( corr500 * invEff );
    }
    epsRel = nReduced/epsRel;
    // ====================================

    // BsPi mass fit, fixed mean and gamma_D0
    TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*800, ( fitPunzi ? 2 : 1 ) * 600);

    ModelPDF *massModel;
    ModelPDF *massErrorModel;
    if(!usePCE){
        suffix += "_tripleGauss";
        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "tripleGauss");
        massModel->setMassBackgroundPDF("massBck", bckType);
        massModel->setPars("massSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_fraction1", 0.17, true);
        massModel->setPars("massRes_fraction2", 0.18, true);
        massModel->setPars("massRes_sigma1", 1.51*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma2", 6.48*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma3", 3.06*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    else{
        suffix += "_PCE";
        massErrorModel = new ModelPDF("massError", data);
        massErrorModel->setMassSignalErrorPDF("massErrorSig", "3Gamma");
        if ( fitPunzi ) {
            can1->Divide(1, 2);
            can1->cd(2);
            massErrorModel->setPars("massErrorSig_a1", 3.);
            massErrorModel->setPars("massErrorSig_a2", 1.);
            massErrorModel->setPars("massErrorSig_a3", 5.);
            massErrorModel->setPars("massErrorSig_b1", 1.);
            massErrorModel->setPars("massErrorSig_b2", 3.);
            massErrorModel->setPars("massErrorSig_b3", 2.);
            massErrorModel->setPars("massErrorSig_c1", 0., true);
            massErrorModel->setPars("massErrorSig_c2", .1);
            massErrorModel->setPars("massErrorSig_c3", 0., true);
            massErrorModel->fit();
            massErrorModel->printResults();
            massErrorModel->drawPlot(MASSERROR, 150, "Mass Error in All Data");
            //can1->SaveAs(name+suffix+".png");
        } else {
            if ( BsPtInt == 10. ) {
                massErrorModel->setPars("massErrorSig_a1", 3.5148, true);
                massErrorModel->setPars("massErrorSig_a2", 0.9040, true);
                massErrorModel->setPars("massErrorSig_a3", 4.2344, true);
                massErrorModel->setPars("massErrorSig_b1", 1.07368, true);
                massErrorModel->setPars("massErrorSig_b2", 3.42336, true);
                massErrorModel->setPars("massErrorSig_b3", 0.9777, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.1005, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.3345, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.6367, true);
            } else if ( BsPtInt == 15. ) {
                massErrorModel->setPars("massErrorSig_a1", 10.54, true);
                massErrorModel->setPars("massErrorSig_a2", 1.516, true);
                massErrorModel->setPars("massErrorSig_a3", 5.35, true);
                massErrorModel->setPars("massErrorSig_b1", 0.4313, true);
                massErrorModel->setPars("massErrorSig_b2", 2.229, true);
                massErrorModel->setPars("massErrorSig_b3", 2.405, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.0372, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.1758, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.9684, true);
            } else {
                cout << "ERROR: Missing Punzi parameters." << endl;
                return;
            }
        }
        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "gaussPCE");
        massModel->setMassSignalErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundPDF("massBck", bckType ); 
        //return;
        massModel->setPars("massSig_mean", mean, true); // D0
        massModel->setPars("massSig_gamma", 21.9, true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_scaleFactor", ( SF + ( systSF * sigmaSF ) ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        
        massModel->setPars(massErrorModel, true);
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    //massModel->fit();

    //can1->cd(1);
    //massModel->drawPlot(MASS, 150);
    //ATLASLabel(0.4,0.52,"Internal");
    //myText(0.4,0.46, 1, "#sqrt{s}=7 TeV, 4.9 fb^{-1}");
    //myText(0.4,0.40, 1, "#sqrt{s}=8 TeV, 19.5 fb^{-1}");
    //can1->SaveAs(name+suffix+".png");
//     can1->SaveAs(name+suffix+".eps");
//     can1->SaveAs(name+suffix+".pdf");
    delete can1;

    cout << "\n============================================================================================================\n" << endl;

    //massModel->printModel();
    //massModel->printResults("const");

    cout << "INFO: BsPi Mass = " << mean << " MeV\n      calcMassErr = " << calcMassErr << " MeV\n      range = ( " << min << " ; " << max << " ) MeV\n      epsRel = " << epsRel << "\n      nSig = " << massModel->getPars("nSig")->getVal() << " +/- " << massModel->getPars("nSig")->getError() << endl;

    //--------------------------------------------------------
    RooWorkspace *Workspace = new RooWorkspace("Workspace","workspace") ;
    //data->saveToWorkspace(Workspace);
    massModel->saveToWorkspace(Workspace);
    Workspace->Print();
    
    RooAbsData *BspiData = Workspace->data("data");
    RooRealVar *BspiMass = Workspace->var("BsPiSubMass");
    RooAbsReal *BspiSignalEvents = Workspace->var("mass_nSig");
    RooAbsReal *BspiBackgroundEvents = Workspace->var("mass_nBck");

    RooRealVar *mass_massBck_a    = Workspace->var("mass_massBck_a");
    RooRealVar *mass_massBck_norm = Workspace->var("mass_massBck_norm");
    RooRealVar *mass_massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *mass_massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *mass_massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *mass_massBck_p4   = Workspace->var("mass_massBck_p4");

    RooRealVar *massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *massBck_p4   = Workspace->var("mass_massBck_p4");
    RooRealVar *massBck_p5   = Workspace->var("mass_massBck_p5");
    RooRealVar *massBck_p6   = Workspace->var("mass_massBck_p6");
    RooRealVar *massBck_p7   = Workspace->var("mass_massBck_p7");

    RooRealVar *RefittedBsPimassErr   = Workspace->var("RefittedBsPimassErr"); ///!!!!!
    //Rho add
    Double_t nBs_central = 0;
    Double_t nBs_central_err = 0;
    Double_t eRel_central = epsRel;
    Double_t eRel_central_err = 0;

    if(BsPtInt == 10){
        if(year == "2012"){
            nBs_central       = 47649.8; 
            nBs_central_err  = sqrt(pow(260.026,2)+pow(1871,2));
        } 
        else if(year == "2011") {
            nBs_central       =  5113.47;// 5032
            nBs_central_err  = sqrt(pow(179.139,2)+pow(80,2));
        }
        else if(year == "2011and2012"){
            nBs_central       = 52750.0; //53794.4
            nBs_central_err  = sqrt(pow(276.0,2)+pow(1044,2));   
        }
        //eRel_central_err  = 1.0/2.1*sqrt(0.17*0.17+0.007*0.007);
        eRel_central_err  = eRel_central*0.17;
    }
    else if(BsPtInt == 15){
        nBs_central       = 43460; ///43497.8
        //nBs_central_err   = 237; //414.617
        nBs_central_err  = sqrt(pow(237,2)+pow(1123,2));
        //eRel_central_err  = 1/1.88 * sqrt(0.19*0.19+0.007*0.007);
        eRel_central_err  = eRel_central*0.19;
    } 

    Workspace->factory("rho[0.00, -1.0, 1.0]");
    Workspace->factory(TString::Format("nBs[%f, 0.0, %f]",nBs_central,nBs_central));//15GeV
    Workspace->factory(TString::Format("eRel[%f, 0.0, %f]",eRel_central,eRel_central));
    Workspace->factory(TString::Format("nSpurious[0.0, %f, %f]",-35.0,35.0));

    if(useSpurious) Workspace->factory("cexpr::BspiSignalEvents('rho * nBs * eRel - nSpurious', rho, nBs, eRel, nSpurious)");
    else            Workspace->factory("cexpr::BspiSignalEvents('rho * nBs * eRel', rho, nBs, eRel)");
    
    if(usePCE)  Workspace->factory("SUM::BspiPDF(BspiSignalEvents * condMassSignal, mass_nBck * condMassBackground)");
    else        Workspace->factory("SUM::BspiPDF(BspiSignalEvents * convMassSignal, mass_nBck * mass_massBck)");
     
    RooAbsPdf *BspiPDF = Workspace->pdf("BspiPDF");

    RooRealVar *rho = Workspace->var("rho");
    RooRealVar *nBs = Workspace->var("nBs");
    RooRealVar *eRel = Workspace->var("eRel");
    RooRealVar *nSpurious = Workspace->var("nSpurious");


    Workspace->factory(TString::Format("RooGaussian::nBsConstraint(nBs, %f, %f)",nBs_central, nBs_central_err));
    Workspace->factory(TString::Format("RooGaussian::eRelConstraint(eRel, %f, %f)",eRel_central, eRel_central_err));
    Workspace->factory(TString::Format("RooGaussian::nSigConstraint(nSpurious, %f, %f)",0.0, 35.0));
    
    if(useSpurious)        Workspace->factory("PROD::FullPDF(BspiPDF, nBsConstraint, eRelConstraint, nSigConstraint)");
    else                        Workspace->factory("PROD::FullPDF(BspiPDF, nBsConstraint, eRelConstraint)");

    RooAbsPdf *FullPDF = Workspace->pdf("FullPDF");

    nBs->setConstant(kTRUE);
    eRel->setConstant(kTRUE);
    rho->setConstant(kTRUE);
    nSpurious->setConstant(kTRUE);
    //BspiPDF->fitTo(*BspiData, NumCPU(4, 0));
    BspiPDF->fitTo(*BspiData);
    Double_t nFit = rho->getVal();
    rho->setConstant(kFALSE);
    nBs->setConstant(kFALSE);
    eRel->setConstant(kFALSE);

    if(nSpurious)nSpurious->setConstant(kFALSE);

    if(bckType == "rooAtlas")
    { 
        mass_massBck_a->setConstant(kTRUE);
        mass_massBck_norm->setConstant(kTRUE);
        mass_massBck_p1->setConstant(kTRUE);
        mass_massBck_p2->setConstant(kTRUE);
        mass_massBck_p3->setConstant(kTRUE);
        mass_massBck_p4->setConstant(kTRUE);
    }
    else if (bckType == "chebychev(7)"){
        massBck_p1->setConstant(kTRUE);
        massBck_p2->setConstant(kTRUE);
        massBck_p3->setConstant(kTRUE);
        massBck_p4->setConstant(kTRUE);
        massBck_p5->setConstant(kTRUE);
        massBck_p6->setConstant(kTRUE);
        massBck_p7->setConstant(kTRUE);
    }
    
    ModelConfig Model("_Model", Workspace);
    //RefittedBsPimassErr->setConstant(kTRUE);    ///!!!!!
    if(usePCE){
        Model.SetObservables(RooArgSet(*BspiMass, *RefittedBsPimassErr));    
    }
    else{
        Model.SetObservables(RooArgSet(*BspiMass));
    }
    Model.SetParametersOfInterest(*rho);

    if(useSpurious) Model.SetNuisanceParameters(RooArgSet(*nBs, *eRel, *BspiBackgroundEvents, *nSpurious));
    else            Model.SetNuisanceParameters(RooArgSet(*nBs, *eRel, *BspiBackgroundEvents));

    Model.SetPdf(*FullPDF);
    
    TCanvas C("C", "C", 800, 800);
    RooPlot *BspiMassPlot = BspiMass->frame(Title("BspiMass"));

    BspiData->plotOn(BspiMassPlot);
    BspiPDF->plotOn(BspiMassPlot, LineColor(kBlack));
    if(usePCE){
        BspiPDF->plotOn(BspiMassPlot, Components("condMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("condMassBackground"), LineColor(kRed));    
    }else{
        BspiPDF->plotOn(BspiMassPlot, Components("convMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("mass_massBck"), LineColor(kRed));    
    }
    
    BspiMassPlot->Draw();
    cout << "INFO: Printing Image2" <<endl;
    C.SaveAs(name+suffix+"Rho.png");

    ModelConfig *SBModel = Model.Clone();
    SBModel->SetName("SBModel");
    ModelConfig *BModel = Model.Clone();
    BModel->SetName("BModel");
    
    RooRealVar *PoI = dynamic_cast<RooRealVar *>(Model.GetParametersOfInterest()->first());
    PoI->setVal(0.1);
    SBModel->SetSnapshot(*PoI);
    PoI->setVal(0.0);
    BModel->SetSnapshot(*PoI);
    
    //AsymptoticCalculator::SetPrintLevel(-1);
    AsymptoticCalculator Calculator(*BspiData, *BModel, *SBModel);
    Calculator.SetOneSided(true);
    
    HypoTestInverter TestInverter(Calculator);
    TestInverter.SetConfidenceLevel(0.95);
    TestInverter.UseCLs(true);
    TestInverter.SetVerbose(false);
    
    TestInverter.SetFixedScan(21, 0.0, 0.05);
    HypoTestInverterResult *Result = TestInverter.GetInterval();
    
    Double_t Down2, Down, Mean, Up, Up2, Limit;
    Down2 = Result->GetExpectedUpperLimit(-2);
    Down = Result->GetExpectedUpperLimit(-1);
    Mean = Result->GetExpectedUpperLimit(0);
    Up = Result->GetExpectedUpperLimit(1);
    Up2 = Result->GetExpectedUpperLimit(2);
    Limit = Result->UpperLimit();
    
    cout << endl <<"LIMIT:"<<mean<<";"<< nFit << ";" << Down2 << ";" << Down << ";" << Mean << ";" << Up << ";" << Up2 << ";" << Limit<<";" << endl;
    cout << endl <<"RES:"<<usePCE<<";"<<useSpurious<<";"<<barrel<<";"<<endcap<<";"<<massSyst<<";"<<gammaSyst<<";"<<typeBW<< ";"<< bckType<< ";"<<Limit<<";"<<endl;

    Workspace->Print();
    //--------------------------------------------------------

    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;

    return;
}

void massScanBsPiRun2(const TString &name = "results/BsPi2015and2016RD_massScan", const Double_t &BsPtInt = 10., const Double_t &mean = 5567.8, const TString typeBW = "Swave", const TString bckType = "rooAtlas", const Double_t &systSF = 0., const Bool_t &fitPunzi = false, Bool_t usePCE = false, Bool_t useSpurious = false, Bool_t barrel = true, Bool_t endcap = true, Int_t massSyst = 0, Int_t gammaSyst = 0, Bool_t useBinnedEpsRel=true, TString year = "2016" ) {
    const time_t startTime = TTimeStamp().GetSec();
    SetAtlasStyle();
    checkDir(name);
    TString suffix = Form("_BpTgt%.0fGeV_mean%.2fMeV_systSF%.0f", BsPtInt, mean, systSF);

    const Double_t SF      = 1.07;
    const Double_t sigmaSF = 0.04;
    const TString cut      = Form("B_pT > %.0f && BsPiSubMass < 5900 && had_3pT > 1000 && had_4pT > 1000 && had_5pT > 500 && BsTau > 0.2 && Track5FromPV && bestChiBs && BsMass > %.0f && BsMass < %.0f", BsPtInt * 1000., signalBandBsMin, signalBandBsMax); 
    suffix += "_defaultCuts";


    cout <<"INFO: FitModel" <<endl;
    cout <<"\t\t Bck Type: "<<bckType <<endl;
    cout <<"\t\t Sig Type: "<<typeBW<<endl;
    cout <<"\t\t usePCE: "<<usePCE <<endl;
    cout <<"\t\t useSpurious: "<<useSpurious <<endl;
    cout <<"\t\t barrel: "<<barrel <<endl;
    cout <<"\t\t endcap: "<<endcap <<endl;
    cout <<"\t\t massSyst: "<<massSyst <<endl;
    cout <<"\t\t gammaSyst: "<<gammaSyst <<endl;
    cout <<"\t\t useBinnedEpsRel: "<<useBinnedEpsRel <<endl;

    suffix += "_"+typeBW;

    if(usePCE){
        cout << "ERROR: Per candidate fit for RUN2 not supported!" <<endl;
        return;
    }

    Observables *observables = new Observables("observables");
    observables->set(MASS, "BsPiSubMass",  "#font[52]{m}(#font[52]{B_{s}}^{0}#font[152]{#pi}^{#pm})", PDGBsMass+PDGpiMass, PDGBsMass+PDGpiMass + 300.);
    RooRealVar *BsPt = new RooRealVar("B_pT", "#font[52]{B_{s}}^{0} #font[52]{p}_{T}", 0., 15e4, "MeV"); // for epsRel
    observables->add(BsPt); // for epsRel

    Data *data15 = new Data("data15", "data15Tracks_nopv_v3.root", "PVTracksComb PVTracksComb_extra", observables, cut);
    Data *data16 = new Data("data16", "data16allYear_BsOnlyTree.root", "PVTracksComb PVTracksComb_extra", observables, cut);

    Data *data;
    if(year == "2015") data = data15;
    else if(year == "2016") data = data16;
    else if(year == "2015and2016"){
        data = data15;
        data->add(data16);
    }
    else {
        cout << "ERROR: Invalid input year!"<< endl;
        return;
    }
    data->SetName("data");

    // Mass window
    // ====================================
    // RelMassError function
    Double_t r0 = -7.76044e+3;
    Double_t r1 =  5.41293e+3;
    Double_t r2 =  1.40965e-1;
    Double_t r3 =  3.16440e-2;
    Double_t r4 = -1.78405e-6;
    Double_t calcMassErr = ( mean - 5366.6 ) * ( r0 * TMath::Landau( mean, r1, r2 ) + ROOT::Math::Chebyshev1( mean, r3, r4 ) );

    // BW(mean, gamma_D0) (x) gauss(0., calcMassErr); signal PDF shape only, no fitting
    ModelPDF *massWindowModel = new ModelPDF("massWindow", data);
    massWindowModel->setMassSignalPDF("massWindowSig", "relBW"+typeBW);
    massWindowModel->setMassResolutionPDF("massWindowRes", "gauss");
    
    /*massWindowModel->setPars("massWindowSig_mean", mean, true); // D0
    massWindowModel->setPars("massWindowSig_gamma", D0gamma, true); // D0
    */
    massWindowModel->setPars("massWindowSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
    massWindowModel->setPars("massWindowSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0    
    massWindowModel->setPars("massWindowSig_massA", PDGBsMass, true);
    massWindowModel->setPars("massWindowSig_massB", PDGpiMass, true);
    massWindowModel->setPars("massWindowRes_mean", 0., true);
    massWindowModel->setPars("massWindowRes_sigma", ( SF + ( systSF * sigmaSF ) ) * calcMassErr, true);

    // BsPi mass range - signal = PDF_value > 0.05 max_PDF_value; max_PDF_value != PDF_value(@mean), I don't know why...
    Int_t nSteps = 3000;
    Double_t maxPdfVal = 0.;
    Double_t pdfVal = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        observables->get(MASS)->setVal( 5500. + (Double_t)i / 10.0 );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        maxPdfVal = ( pdfVal > maxPdfVal ? pdfVal : maxPdfVal );
    }
    Double_t min = 0.;
    Double_t max = 0.;
    Double_t val = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        val = 5500. + (Double_t)i / 10.0;
        observables->get(MASS)->setVal( val );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        if ( min == 0. ) {
            if ( pdfVal / maxPdfVal >= 0.05 ) min = val;
        } else if ( max == 0. ) {
            if ( pdfVal / maxPdfVal <= 0.05 ) max = val;
        }
    }
    if ( min < observables->get(MASS)->getMin() || max > observables->get(MASS)->getMax() || min == 0. || max == 0. ) {
        cout << "INFO: Mass window ( " << min << " ; " << max << " ) MeV is out of the data range ( " << observables->get(MASS)->getMin() << " ; " << observables->get(MASS)->getMax() << " ) MeV." << endl;
        return;
    }
    // calculate "corr500 * 1/corrPion" for each Bs_pT in this (BsPi mass) range, make an average and inverse value ( = epsRel)
    TString reduceCut = "BsPiSubMass > ";
    reduceCut += min;
    reduceCut += " && BsPiSubMass < ";
    reduceCut += max;
    Data *dataReduced = data->reduce("dataReduced", reduceCut);

    Long64_t nReduced = dataReduced->getDataset()->numEntries();

    Double_t p0[3];
    Double_t p1[3];
    Double_t p2[3];
    Double_t p3[3];
    Double_t p4[3];
    Double_t q0[3];
    Double_t q1[3];
    Double_t q2[3];
    Double_t q3[3];

    if (barrel && endcap){
        if(useBinnedEpsRel){
            p0[0] =  5.31903e+0;
            p1[0] = -4.08609e-4;
            p2[0] =  2.48093e+0;
            p3[0] = -6.89892e-5;
            p4[0] =  3.98142e-10;
            q0[0] =  2.69890e+0;
            q1[0] = -2.59078e-4;
            q2[0] =  1.32936e+0;
            q3[0] = -6.60074e-6;

            p0[1] =  3.43427e+0;
            p1[1] = -3.16100e-4;
            p2[1] =  2.03849e+0;
            p3[1] = -4.30101e-5;
            p4[1] =  2.24453e-10;
            q0[1] =  1.57724e+0;
            q1[1] = -3.01711e-4;
            q2[1] =  1.44780e+0;
            q3[1] = -6.87981e-6;
                
            p0[2] =  2.50898e+0;
            p1[2] = -2.67756e-4;
            p2[2] =  1.75799e+0;
            p3[2] = -2.77793e-5;
            p4[2] =  1.32081e-10;
            q0[2] = -8.43088e+0;
            q1[2] = -7.13354e-4;
            q2[2] =  1.37236e+0;
            q3[2] = -2.73718e-6;
        }
        else{
            p0[0] = p0[1] = p0[2] =  3.88384e+0;
            p1[0] = p1[1] = p1[2] = -3.52755e-4;
            p2[0] = p2[1] = p2[2] =  2.12907e+0;
            p3[0] = p3[1] = p3[2] = -4.80883e-5;
            p4[0] = p4[1] = p4[2] =  2.58203e-10;
            q0[0] = q0[1] = q0[2] =  9.76032e-1;
            q1[0] = q1[1] = q1[2] = -2.04753e-4;
            q2[0] = q2[1] = q2[2] =  1.35443e+0;
            q3[0] = q3[1] = q3[2] = -4.38256e-6;
        }
    }
    else if(barrel){ 
        if(useBinnedEpsRel){
            p0[0] =  4.93495e+0;
            p1[0] = -3.81853e-4;
            p2[0] =  2.30788e+0;
            p3[0] = -5.93104e-5;
            p4[0] =  3.33016e-10;
            q0[0] =  2.11580e+0;
            q1[0] = -2.09079e-4;
            q2[0] =  1.18319e+0;
            q3[0] = -3.95755e-6;
                    
            p0[1] =  3.24873e+0;
            p1[1] = -3.03512e-4;
            p2[1] =  1.93239e+0;
            p3[1] = -3.81541e-5;
            p4[1] =  1.97590e-10;
            q0[1] =  2.19535e+0;
            q1[1] = -3.41660e-4;
            q2[1] =  1.36806e+0;
            q3[1] = -6.34577e-6;
            
            p0[2] =  1.88642e+0;
            p1[2] = -2.12971e-4;
            p2[2] =  1.50657e+0;
            p3[2] = -1.43228e-5;
            p4[2] =  4.47887e-11;
            q0[2] =  -8.61924e+0;
            q1[2] = -6.38711e-4;
            q2[2] =  1.32552e+0;
            q3[2] = -3.07364e-6;
        }
        else{
            p0[0] = p0[1] = p0[2] =  3.71273e+0;
            p1[0] = p1[1] = p1[2] = -3.46605e-4;
            p2[0] = p2[1] = p2[2] =  2.05107e+0;
            p3[0] = p3[1] = p3[2] = -4.41165e-5;
            p4[0] = p4[1] = p4[2] =  2.33609e-10;
            q0[0] = q0[1] = q0[2] =  1.00542e+0;
            q1[0] = q1[1] = q1[2] = -2.05837e-4;
            q2[0] = q2[1] = q2[2] =  1.28288e+0;
            q3[0] = q3[1] = q3[2] = -3.86472e-6;
        }
    }
    else if(endcap){ 
        if(useBinnedEpsRel){
            p0[0] =  5.63178e+0;
            p1[0] = -4.32112e-4;
            p2[0] =  2.62223e+0;
            p3[0] = -7.71731e-5;
            p4[0] =  4.54671e-10;
            q0[0] =  3.46251e+0;
            q1[0] = -3.19069e-4;
            q2[0] =  1.47281e+0;
            q3[0] = -9.44006e-6;
            
            p0[1] =  3.54207e+0;
            p1[1] = -3.22886e-4;
            p2[1] =  2.10342e+0;
            p3[1] = -4.59175e-5;
            p4[1] =  2.39939e-10;
            q0[1] =  2.19535e+0;
            q1[1] = -3.41660e-4;
            q2[1] =  1.36806e+0;
            q3[1] = -6.34577e-6;
    
            p0[2] =  3.06070e+0;
            p1[2] = -3.16608e-4;
            p2[2] =  1.95728e+0;
            p3[2] = -3.93035e-5;
            p4[2] =  2.11573e-10;
            q0[2] =  -1.59393e+0;
            q1[2] = -6.13077e-5;
            q2[2] =  1.32146e+0;
            q3[2] = -2.61075e-7;
            }
            else{
                p0[0] = p0[1] = p0[2] =  4.00544e+0;
                p1[0] = p1[1] = p1[2] = -3.57902e-4;
                p2[0] = p2[1] = p2[2] =  2.18582e+0;
                p3[0] = p3[1] = p3[2] = -5.10119e-5;
                p4[0] = p4[1] = p4[2] =  2.76416e-10;
                q0[0] = q0[1] = q0[2] =  1.20435e+0;
                q1[0] = q1[1] = q1[2] = -2.17018e-4;
                q2[0] = q2[1] = q2[2] =  1.43371e+0;
                q3[0] = q3[1] = q3[2] = -4.96948e-6;
        }
    }
    else{ 
        cout << "ERROR:No barrel or endcap selected." << endl;
        return; 
    }

    Double_t epsRel = 0.;
    for ( Long64_t j = 0; j < nReduced; j++ ) {
        Double_t x = dataReduced->getDataset()->get( j )->getRealValue("B_pT", -999.);
        Double_t BsPi_mass = dataReduced->getDataset()->get( j )->getRealValue("BsPiSubMass", -999.);
        if ( x == -999. ) {
            cout << "ERROR: Can't read Bs pT!" << endl;
            return;
        }
        if ( BsPi_mass == -999. ) {
            cout << "ERROR: Can't read BsPi mass!" << endl;
            return;
        }
        Double_t corr500 = 0;
        Double_t invEff = 0;

        if(BsPi_mass<5560){
            corr500 = exp( p0[0] + p1[0] * x ) + ROOT::Math::Chebyshev2( x, p2[0], p3[0], p4[0] );
            invEff = TMath::Exp( q0[0] + q1[0] * x ) + ROOT::Math::Chebyshev1( x, q2[0], q3[0] );
        }
        else if(BsPi_mass<5575){
            corr500 = exp( p0[1] + p1[1] * x ) + ROOT::Math::Chebyshev2( x, p2[1], p3[1], p4[1] );
            invEff = TMath::Exp( q0[1] + q1[1] * x ) + ROOT::Math::Chebyshev1( x, q2[1], q3[1] );
        }
        else{
            corr500 = exp( p0[2] + p1[2] * x ) + ROOT::Math::Chebyshev2( x, p2[2], p3[2], p4[2] );
            invEff = TMath::Exp( q0[2] + q1[2] * x ) + ROOT::Math::Chebyshev1( x, q2[2], q3[2] );
        }

        epsRel += ( corr500 * invEff );
    }
    epsRel = nReduced/epsRel;
    // ====================================

    // BsPi mass fit, fixed mean and gamma_D0
    TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*800, ( fitPunzi ? 2 : 1 ) * 600);

    ModelPDF *massModel;
    ModelPDF *massErrorModel;

    if(!usePCE){
        suffix += "_tripleGauss";
        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "tripleGauss");
        massModel->setMassBackgroundPDF("massBck", bckType);
        massModel->setPars("massSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_fraction1", 0.17, true);
        massModel->setPars("massRes_fraction2", 0.18, true);
        massModel->setPars("massRes_sigma1", 1.51*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma2", 6.48*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma3", 3.06*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    else{
        suffix += "_PCE";
        massErrorModel = new ModelPDF("massError", data);
        massErrorModel->setMassSignalErrorPDF("massErrorSig", "3Gamma");
        if ( fitPunzi ) {
            can1->Divide(1, 2);
            can1->cd(2);
            massErrorModel->setPars("massErrorSig_a1", 3.);
            massErrorModel->setPars("massErrorSig_a2", 1.);
            massErrorModel->setPars("massErrorSig_a3", 5.);
            massErrorModel->setPars("massErrorSig_b1", 1.);
            massErrorModel->setPars("massErrorSig_b2", 3.);
            massErrorModel->setPars("massErrorSig_b3", 2.);
            massErrorModel->setPars("massErrorSig_c1", 0., true);
            massErrorModel->setPars("massErrorSig_c2", .1);
            massErrorModel->setPars("massErrorSig_c3", 0., true);
            massErrorModel->fit();
            massErrorModel->printResults();
            massErrorModel->drawPlot(MASSERROR, 150, "Mass Error in All Data");
            //can1->SaveAs(name+suffix+".png");
        } else {
            if ( BsPtInt == 10. ) {
                massErrorModel->setPars("massErrorSig_a1", 3.5148, true);
                massErrorModel->setPars("massErrorSig_a2", 0.9040, true);
                massErrorModel->setPars("massErrorSig_a3", 4.2344, true);
                massErrorModel->setPars("massErrorSig_b1", 1.07368, true);
                massErrorModel->setPars("massErrorSig_b2", 3.42336, true);
                massErrorModel->setPars("massErrorSig_b3", 0.9777, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.1005, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.3345, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.6367, true);
            } else if ( BsPtInt == 15. ) {
                massErrorModel->setPars("massErrorSig_a1", 10.54, true);
                massErrorModel->setPars("massErrorSig_a2", 1.516, true);
                massErrorModel->setPars("massErrorSig_a3", 5.35, true);
                massErrorModel->setPars("massErrorSig_b1", 0.4313, true);
                massErrorModel->setPars("massErrorSig_b2", 2.229, true);
                massErrorModel->setPars("massErrorSig_b3", 2.405, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.0372, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.1758, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.9684, true);
            } else {
                cout << "ERROR: Missing Punzi parameters." << endl;
                return;
            }
        }
        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "gaussPCE");
        massModel->setMassSignalErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundPDF("massBck", bckType ); 
        massModel->setPars("massSig_mean", mean, true); // D0
        massModel->setPars("massSig_gamma", 21.9, true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_scaleFactor", ( SF + ( systSF * sigmaSF ) ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        
        massModel->setPars(massErrorModel, true);
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    //massModel->fit();

    //can1->cd(1);
    //massModel->drawPlot(MASS, 150);
    //ATLASLabel(0.4,0.52,"Internal");
    //myText(0.4,0.46, 1, "#sqrt{s}=7 TeV, 4.9 fb^{-1}");
    //myText(0.4,0.40, 1, "#sqrt{s}=8 TeV, 19.5 fb^{-1}");
    //can1->SaveAs(name+suffix+".png");
//     can1->SaveAs(name+suffix+".eps");
//     can1->SaveAs(name+suffix+".pdf");
    delete can1;

    cout << "\n============================================================================================================\n" << endl;

    //massModel->printModel();
    //massModel->printResults("const");

    cout << "INFO: BsPi Mass = " << mean << " MeV\n      calcMassErr = " << calcMassErr << " MeV\n      range = ( " << min << " ; " << max << " ) MeV\n      epsRel = " << epsRel << "\n      nSig = " << massModel->getPars("nSig")->getVal() << " +/- " << massModel->getPars("nSig")->getError() << endl;

    //--------------------------------------------------------
    RooWorkspace *Workspace = new RooWorkspace("Workspace","workspace") ;
    //data->saveToWorkspace(Workspace);
    massModel->saveToWorkspace(Workspace);
    Workspace->Print();
    
    RooAbsData *BspiData = Workspace->data("data");
    RooRealVar *BspiMass = Workspace->var("BsPiSubMass");
    RooAbsReal *BspiSignalEvents = Workspace->var("mass_nSig");
    RooAbsReal *BspiBackgroundEvents = Workspace->var("mass_nBck");

    RooRealVar *mass_massBck_a    = Workspace->var("mass_massBck_a");
    RooRealVar *mass_massBck_norm = Workspace->var("mass_massBck_norm");
    RooRealVar *mass_massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *mass_massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *mass_massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *mass_massBck_p4   = Workspace->var("mass_massBck_p4");

    RooRealVar *massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *massBck_p4   = Workspace->var("mass_massBck_p4");
    RooRealVar *massBck_p5   = Workspace->var("mass_massBck_p5");
    RooRealVar *massBck_p6   = Workspace->var("mass_massBck_p6");
    RooRealVar *massBck_p7   = Workspace->var("mass_massBck_p7");

    //Rho add
    Double_t nBs_central = 0;
    Double_t nBs_central_err = 0;
    Double_t eRel_central = epsRel;
    Double_t eRel_central_err = 0;

    if(BsPtInt == 10){
        if(year == "2015"){
            nBs_central       = 11536.1; //12046.4
            nBs_central_err  = sqrt(pow(121.365,2)+pow(12046.4-11536.1,2));
        } 
        else if(year == "2016") {
            nBs_central       =  76669.8;// 79425.5.
            nBs_central_err  = sqrt(pow(320.606,2)+pow(76669.8-79425.5,2));
        }
        else if(year == "2015and2016"){
            nBs_central       = 30131.4; //31493.7
            nBs_central_err  = sqrt(pow(209.835,2)+pow(31493.7-30131.4,2));   
        }
        //eRel_central_err  = 1.0/2.1*sqrt(0.17*0.17+0.007*0.007);
        eRel_central_err  = eRel_central*0.17;
    }
    else if(BsPtInt == 15){
        nBs_central       = 43460; ///43497.8
        nBs_central_err  = sqrt(pow(237,2)+pow(1123,2));
        //eRel_central_err  = 1/1.88 * sqrt(0.19*0.19+0.007*0.007);
        eRel_central_err  = eRel_central*0.19;
    } 

    Workspace->factory("rho[0.00, -1.0, 1.0]");
    Workspace->factory(TString::Format("nBs[%f, 0.0, %f]",nBs_central,nBs_central));//15GeV
    Workspace->factory(TString::Format("eRel[%f, 0.0, %f]",eRel_central,eRel_central));
    Workspace->factory(TString::Format("nSpurious[0.0, %f, %f]",-35.0,35.0));

    if(useSpurious) Workspace->factory("cexpr::BspiSignalEvents('rho * nBs * eRel - nSpurious', rho, nBs, eRel, nSpurious)");
    else            Workspace->factory("cexpr::BspiSignalEvents('rho * nBs * eRel', rho, nBs, eRel)");
    
    if(usePCE)  Workspace->factory("SUM::BspiPDF(BspiSignalEvents * condMassSignal, mass_nBck * condMassBackground)");
    else        Workspace->factory("SUM::BspiPDF(BspiSignalEvents * convMassSignal, mass_nBck * mass_massBck)");
    
    RooAbsPdf *BspiPDF = Workspace->pdf("BspiPDF");

    RooRealVar *rho = Workspace->var("rho");
    RooRealVar *nBs = Workspace->var("nBs");
    RooRealVar *eRel = Workspace->var("eRel");
    RooRealVar *nSpurious = Workspace->var("nSpurious");


    Workspace->factory(TString::Format("RooGaussian::nBsConstraint(nBs, %f, %f)",nBs_central, nBs_central_err));
    Workspace->factory(TString::Format("RooGaussian::eRelConstraint(eRel, %f, %f)",eRel_central, eRel_central_err));
    Workspace->factory(TString::Format("RooGaussian::nSigConstraint(nSpurious, %f, %f)",0.0, 35.0));
    
    if(useSpurious)        Workspace->factory("PROD::FullPDF(BspiPDF, nBsConstraint, eRelConstraint, nSigConstraint)");
    else                        Workspace->factory("PROD::FullPDF(BspiPDF, nBsConstraint, eRelConstraint)");

    RooAbsPdf *FullPDF = Workspace->pdf("FullPDF");

    nBs->setConstant(kTRUE);
    eRel->setConstant(kTRUE);
    rho->setConstant(kTRUE);
    nSpurious->setConstant(kTRUE);
    //BspiPDF->fitTo(*BspiData, NumCPU(4, 0));
    BspiPDF->fitTo(*BspiData);
    Double_t nFit = rho->getVal();
    rho->setConstant(kFALSE);
    nBs->setConstant(kFALSE);
    eRel->setConstant(kFALSE);

    if(nSpurious)nSpurious->setConstant(kFALSE);

    if(bckType == "rooAtlas")
    { 
        mass_massBck_a->setConstant(kTRUE);
        mass_massBck_norm->setConstant(kTRUE);
        mass_massBck_p1->setConstant(kTRUE);
        mass_massBck_p2->setConstant(kTRUE);
        mass_massBck_p3->setConstant(kTRUE);
        mass_massBck_p4->setConstant(kTRUE);
    }
    else if (bckType == "chebychev(7)"){
        massBck_p1->setConstant(kTRUE);
        massBck_p2->setConstant(kTRUE);
        massBck_p3->setConstant(kTRUE);
        massBck_p4->setConstant(kTRUE);
        massBck_p5->setConstant(kTRUE);
        massBck_p6->setConstant(kTRUE);
        massBck_p7->setConstant(kTRUE);
    }
    
    ModelConfig Model("_Model", Workspace);
    //RefittedBsPimassErr->setConstant(kTRUE);    ///!!!!!
    if(usePCE){
        Model.SetObservables(RooArgSet(*BspiMass));    
    }
    else{
        Model.SetObservables(RooArgSet(*BspiMass));
    }
    Model.SetParametersOfInterest(*rho);

    if(useSpurious) Model.SetNuisanceParameters(RooArgSet(*nBs, *eRel, *BspiBackgroundEvents, *nSpurious));
    else            Model.SetNuisanceParameters(RooArgSet(*nBs, *eRel, *BspiBackgroundEvents));

    Model.SetPdf(*FullPDF);
    
    TCanvas C("C", "C", 800, 800);
    RooPlot *BspiMassPlot = BspiMass->frame(Title("BspiMass"));

    BspiData->plotOn(BspiMassPlot);
    BspiPDF->plotOn(BspiMassPlot, LineColor(kBlack));
    if(usePCE){
        BspiPDF->plotOn(BspiMassPlot, Components("condMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("condMassBackground"), LineColor(kRed));    
    }else{
        BspiPDF->plotOn(BspiMassPlot, Components("convMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("mass_massBck"), LineColor(kRed));    
    }

    BspiMassPlot->Draw();
    cout << "INFO: Printing Image2" <<endl;
    C.SaveAs(name+suffix+"Rho.png");

    ModelConfig *SBModel = Model.Clone();
    SBModel->SetName("SBModel");
    ModelConfig *BModel = Model.Clone();
    BModel->SetName("BModel");
    
    RooRealVar *PoI = dynamic_cast<RooRealVar *>(Model.GetParametersOfInterest()->first());
    PoI->setVal(0.1);
    SBModel->SetSnapshot(*PoI);
    PoI->setVal(0.0);
    BModel->SetSnapshot(*PoI);
    
    //AsymptoticCalculator::SetPrintLevel(-1);
    AsymptoticCalculator Calculator(*BspiData, *BModel, *SBModel);
    Calculator.SetOneSided(true);
    
    HypoTestInverter TestInverter(Calculator);
    TestInverter.SetConfidenceLevel(0.95);
    TestInverter.UseCLs(true);
    TestInverter.SetVerbose(false);
    
    TestInverter.SetFixedScan(21, 0.0, 0.05);
    HypoTestInverterResult *Result = TestInverter.GetInterval();
    
    Double_t Down2, Down, Mean, Up, Up2, Limit;
    Down2 = Result->GetExpectedUpperLimit(-2);
    Down = Result->GetExpectedUpperLimit(-1);
    Mean = Result->GetExpectedUpperLimit(0);
    Up = Result->GetExpectedUpperLimit(1);
    Up2 = Result->GetExpectedUpperLimit(2);
    Limit = Result->UpperLimit();
    
    cout << endl <<"LIMIT:"<<mean<<";"<< nFit << ";" << Down2 << ";" << Down << ";" << Mean << ";" << Up << ";" << Up2 << ";" << Limit<<";" << endl;
    cout << endl <<"RES:"<<usePCE<<";"<<useSpurious<<";"<<barrel<<";"<<endcap<<";"<<massSyst<<";"<<gammaSyst<<";"<<typeBW<< ";"<< bckType<< ";"<<Limit<<";"<<endl;

    Workspace->Print();
    //--------------------------------------------------------

    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;

    return;
}

void massScanBsPiRhoFinal(Double_t massPiont = 5567.8) {
//    const TString typeBW = "Swave";
     const TString typeBW = "Pwave";
//    const TString bckType = "rooAtlas";
    const TString bckType = "chebychev(7)";
    
    //massPiont = 5650; /// test systematics effect
    Double_t ptCut = 10;
    Bool_t fitPunzi = false;
    Bool_t usePCE = false;  //
    Bool_t useSpurious = false;
    Bool_t barrel = false;   
    Bool_t endcap = true;
    Int_t systSF = 0;
    Int_t massSyst = 1;
    Int_t gammaSyst = 1;
    Bool_t useBinnedEpsRel = true;
    TString year = "2011and2012";
    //TString year = "2011";
    //TString year = "2012";
    //TString year = "2016";

    /*const TString typeBW = "Swave";
    const TString bckType = "rooAtlas";
    
    Double_t ptCut = 10;
    Bool_t fitPunzi = false;
    Bool_t usePCE = true;
    Bool_t useSpurious = false;
    Bool_t barrel = false;   
    Bool_t endcap = true;
    Int_t systSF = 0;
    Int_t massSyst = 0;
    Int_t gammaSyst = 0;
    Bool_t useBinnedEpsRel = true;
    TString year = "2011and2012";*/

    if( (year == "2011") || (year == "2012") || (year == "2011and2012")){
        massScanBsPiRun1("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, useSpurious, barrel, endcap, massSyst, gammaSyst, useBinnedEpsRel, year);
    }
    else if( (year == "2015") || (year == "2016") || (year == "2015and2016")){
        massScanBsPiRun2("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, useSpurious, barrel, endcap, massSyst, gammaSyst, useBinnedEpsRel, year);
    }
    
    //massScanBsPiRun1("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, useSpurious, barrel, endcap, massSyst, gammaSyst, useBinnedEpsRel, year);
    //massScanBsPiRun2("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, useSpurious, barrel, endcap, massSyst, gammaSyst, useBinnedEpsRel, year);
    
    /*for(auto iMassSyst = -1; iMassSyst <= 1;iMassSyst++ ){
        for(auto iGammaSyst = -1; iGammaSyst <= 1;iGammaSyst++ ){
            for (bool iBarrel : { false, true }){
                for (bool iEndcap : { false, true }){
                    for (auto iTypeBW : {"Swave", "Pwave"}){
                        for (auto iBckType : { "rooAtlas" ,"chebychev(7)"  }){
                            massScanBsPiRun1("results/BsPi2011and2012RD_massScan", ptCut, massPiont, iTypeBW, iBckType, systSF, fitPunzi, usePCE, useSpurious, iBarrel, iEndcap, iMassSyst, iGammaSyst, useBinnedEpsRel, year);    
                        }
                    }
                }
            }            
        }
    }*/
    return;
}
