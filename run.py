import os, sys
import shutil
import datetime
import time
import getpass
from multiprocessing import Pool

nCores = 8
		

def func(massPoint):
	print "Doing: "+str(massPoint)+"\""
	workDir = os.getcwd()
	#os.system("root -b -l -q \"massScanBsPiRho.cxx++("+str(massPoint)+")\">> run/nohup"+str(massPoint)+".out")
	os.system("root -b -l -q \"massScanBsPiRhoFinal.cxx++("+str(massPoint)+")\">> run/nohup"+str(massPoint)+".out")
	time.sleep(5)
	return True

if __name__ == '__main__':
	workDir = os.getcwd()
	print workDir
	directory = "run"
	if os.path.exists(workDir+"/"+directory):
		shutil.rmtree(directory)
	os.makedirs(directory)

	pool = Pool(processes=int(nCores))
	#for massPoint in [5568]:
	massPoint = 5550
	step = 10
	while (massPoint <= 5700):
		pool.apply_async(func, args=(massPoint,))
		time.sleep(60)
		massPoint = massPoint + step

	pool.close()
	pool.join()
