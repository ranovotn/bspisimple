#include "BFitCore/IncludeAllSources.h"

#include "RooStats/AsymptoticCalculator.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodTestStat.h"

using namespace RooStats;
using namespace RooFit;
using namespace std;

const Double_t INF = RooNumber::infinity();

const Double_t signalBandBsMin = 5346.; // MeV
const Double_t signalBandBsMax = 5386.; // MeV
const TString leftSideBandBsCut = "BsMass < 5210"; // MeV
const TString rightSideBandBsCut = "BsMass > 5510"; // MeV

const Double_t PDGBsMass = 5366.77; // MeV
const Double_t PDGpiMass = 139.57; // MeV

const Double_t D0mean = 5567.8;
const Double_t D0gamma = 21.9;
const Double_t D0meanErrStat = 2.9;
const Double_t D0gammaErrStat = 6.4;
const Double_t D0meanErrSystUp = 0.9;
const Double_t D0meanErrSystDown = 1.9;
const Double_t D0gammaErrSystUp = 5.0;
const Double_t D0gammaErrSystDown = 2.5;

              
void massScanBsPiRun1(const TString &name = "results/BsPi2011and2012RD_massScan", const Double_t &BsPtInt = 10., const Double_t &mean = 5567.8, const TString typeBW = "Swave", const TString bckType = "rooAtlas", const Double_t &systSF = 0., const Bool_t &fitPunzi = false, Bool_t usePCE = false, Int_t massSyst = 0, Int_t gammaSyst = 0, TString year = "2012") {
    const time_t startTime = TTimeStamp().GetSec();
    SetAtlasStyle();
    checkDir(name);
    TString suffix = Form("_BpTgt%.0fGeV_mean%.2fMeV_systSF%.0f", BsPtInt, mean, systSF);

    const Double_t SF      = 1.07;
    const Double_t sigmaSF = 0.04;
    const TString cut      = Form("B_pT > %.0f && BsPiSubMass < 5900 && had_3pT > 1000 && had_4pT > 1000 && had_5pT > 500 && BsTau > 0.2 && Track5FromPV && bestChiBs && BsMass > %.0f && BsMass < %.0f", BsPtInt * 1000., signalBandBsMin, signalBandBsMax); 
    suffix += "_defaultCuts";


    cout <<"INFO: FitModel" <<endl;
    cout <<"\t\t Bck Type: "<<bckType <<endl;
    cout <<"\t\t Sig Type: "<<typeBW<<endl;
    cout <<"\t\t usePCE: "<<usePCE <<endl;
    cout <<"\t\t massSyst: "<<massSyst <<endl;
    cout <<"\t\t gammaSyst: "<<gammaSyst <<endl;

    suffix += "_"+typeBW;

    

    Observables *observables = new Observables("observables");
    observables->set(MASS, "BsPiSubMass",  "#font[52]{m}(#font[52]{B_{s}}^{0}#font[152]{#pi}^{#pm})", PDGBsMass+PDGpiMass, PDGBsMass+PDGpiMass + 300.);
    observables->set(MASSERROR, "RefittedBsPimassErr", "#font[152]{#sigma}_{#font[52]{m}}(#font[52]{B_{s}}^{0}#font[152]{#pi}^{#pm})", .001, .001 + 30.);
    RooRealVar *BsPt = new RooRealVar("B_pT", "#font[52]{B_{s}}^{0} #font[52]{p}_{T}", 0., 15e4, "MeV"); // for epsRel
    observables->add(BsPt); // for epsRel

    Data *data11 = new Data("data11", "data11cascadeBsPi.bsConstraint.NoBestChi.v10.root", "BsPiCascade CascadeExtraInfo", observables, cut);
    Data *data12 = new Data("data12", "data12cascadeBsPi.bsConstraint.NoBestChi.v10.root", "BsPiCascade CascadeExtraInfo", observables, cut);

    Data *data;
    if(year == "2012") data = data12;
    else if(year == "2011") data = data11;
    else if(year == "2011and2012"){
        data = data12;
        data->add(data11);
    }
    else {
        cout << "ERROR: Invalid input year!"<< endl;
        return;
    }
    data->SetName("data");

    // Mass window
    // ====================================
    // RelMassError function
    Double_t r0 = -7.76044e+3;
    Double_t r1 =  5.41293e+3;
    Double_t r2 =  1.40965e-1;
    Double_t r3 =  3.16440e-2;
    Double_t r4 = -1.78405e-6;
    Double_t calcMassErr = ( mean - 5366.6 ) * ( r0 * TMath::Landau( mean, r1, r2 ) + ROOT::Math::Chebyshev1( mean, r3, r4 ) );

    // BW(mean, gamma_D0) (x) gauss(0., calcMassErr); signal PDF shape only, no fitting
    ModelPDF *massWindowModel = new ModelPDF("massWindow", data);
    massWindowModel->setMassSignalPDF("massWindowSig", "relBW"+typeBW);
    massWindowModel->setMassResolutionPDF("massWindowRes", "gauss");
    massWindowModel->setPars("massWindowSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
    massWindowModel->setPars("massWindowSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0    
    massWindowModel->setPars("massWindowSig_massA", PDGBsMass, true);
    massWindowModel->setPars("massWindowSig_massB", PDGpiMass, true);
    massWindowModel->setPars("massWindowRes_mean", 0., true);
    massWindowModel->setPars("massWindowRes_sigma", ( SF + ( systSF * sigmaSF ) ) * calcMassErr, true);

    // BsPi mass range - signal = PDF_value > 0.05 max_PDF_value; max_PDF_value != PDF_value(@mean), I don't know why...
    Int_t nSteps = 3000;
    Double_t maxPdfVal = 0.;
    Double_t pdfVal = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        observables->get(MASS)->setVal( 5500. + (Double_t)i / 10.0 );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        maxPdfVal = ( pdfVal > maxPdfVal ? pdfVal : maxPdfVal );
    }
    Double_t min = 0.;
    Double_t max = 0.;
    Double_t val = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        val = 5500. + (Double_t)i / 10.0;
        observables->get(MASS)->setVal( val );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        if ( min == 0. ) {
            if ( pdfVal / maxPdfVal >= 0.05 ) min = val;
        } else if ( max == 0. ) {
            if ( pdfVal / maxPdfVal <= 0.05 ) max = val;
        }
    }
    if ( min < observables->get(MASS)->getMin() || max > observables->get(MASS)->getMax() || min == 0. || max == 0. ) {
        cout << "INFO: Mass window ( " << min << " ; " << max << " ) MeV is out of the data range ( " << observables->get(MASS)->getMin() << " ; " << observables->get(MASS)->getMax() << " ) MeV." << endl;
        return;
    }

    // BsPi mass fit, fixed mean and gamma_D0
    TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*800, ( fitPunzi ? 2 : 1 ) * 600);

    ModelPDF *massModel;
    ModelPDF *massErrorModel;

    if(!usePCE){
        suffix += "_tripleGauss";
        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "tripleGauss");
        massModel->setMassBackgroundPDF("massBck", bckType);
        massModel->setPars("massSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_fraction1", 0.17, true);
        massModel->setPars("massRes_fraction2", 0.18, true);
        massModel->setPars("massRes_sigma1", 1.51*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma2", 6.48*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma3", 3.06*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    else{
        suffix += "_PCE";
        massErrorModel = new ModelPDF("massError", data);
        massErrorModel->setMassSignalErrorPDF("massErrorSig", "3Gamma");
        if ( fitPunzi ) {
            can1->Divide(1, 2);
            can1->cd(2);
            massErrorModel->setPars("massErrorSig_a1", 3.);
            massErrorModel->setPars("massErrorSig_a2", 1.);
            massErrorModel->setPars("massErrorSig_a3", 5.);
            massErrorModel->setPars("massErrorSig_b1", 1.);
            massErrorModel->setPars("massErrorSig_b2", 3.);
            massErrorModel->setPars("massErrorSig_b3", 2.);
            massErrorModel->setPars("massErrorSig_c1", 0., true);
            massErrorModel->setPars("massErrorSig_c2", .1);
            massErrorModel->setPars("massErrorSig_c3", 0., true);
            massErrorModel->fit();
            massErrorModel->printResults();
            massErrorModel->drawPlot(MASSERROR, 150, "Mass Error in All Data");
        } else {
            if ( BsPtInt == 10. ) {
                massErrorModel->setPars("massErrorSig_a1", 3.5148, true);
                massErrorModel->setPars("massErrorSig_a2", 0.9040, true);
                massErrorModel->setPars("massErrorSig_a3", 4.2344, true);
                massErrorModel->setPars("massErrorSig_b1", 1.07368, true);
                massErrorModel->setPars("massErrorSig_b2", 3.42336, true);
                massErrorModel->setPars("massErrorSig_b3", 0.9777, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.1005, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.3345, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.6367, true);
            } else if ( BsPtInt == 15. ) {
                massErrorModel->setPars("massErrorSig_a1", 10.54, true);
                massErrorModel->setPars("massErrorSig_a2", 1.516, true);
                massErrorModel->setPars("massErrorSig_a3", 5.35, true);
                massErrorModel->setPars("massErrorSig_b1", 0.4313, true);
                massErrorModel->setPars("massErrorSig_b2", 2.229, true);
                massErrorModel->setPars("massErrorSig_b3", 2.405, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.0372, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.1758, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.9684, true);
            } else {
                cout << "ERROR: Missing Punzi parameters." << endl;
                return;
            }
        }

        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "gaussPCE");
        massModel->setMassSignalErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundPDF("massBck", bckType ); 
        massModel->setPars("massSig_mean", mean, true); // D0
        massModel->setPars("massSig_gamma", 21.9, true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_scaleFactor", ( SF + ( systSF * sigmaSF ) ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        
        massModel->setPars(massErrorModel, true);
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    delete can1;

    cout << "\n============================================================================================================\n" << endl;

    //--------------------------------------------------------
    RooWorkspace *Workspace = new RooWorkspace("Workspace","workspace") ;
    //data->saveToWorkspace(Workspace);
    massModel->saveToWorkspace(Workspace);
    Workspace->Print();
    
    RooAbsData *BspiData = Workspace->data("data");
    RooRealVar *BspiMass = Workspace->var("BsPiSubMass");
    RooRealVar *BspiSignalEvents = Workspace->var("mass_nSig");
    RooRealVar *BspiBackgroundEvents = Workspace->var("mass_nBck");
    RooAbsPdf *BspiPDF = Workspace->pdf("totalModel");


    RooRealVar *mass_massBck_a    = Workspace->var("mass_massBck_a");
    RooRealVar *mass_massBck_norm = Workspace->var("mass_massBck_norm");
    RooRealVar *mass_massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *mass_massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *mass_massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *mass_massBck_p4   = Workspace->var("mass_massBck_p4");

    RooRealVar *massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *massBck_p4   = Workspace->var("mass_massBck_p4");
    RooRealVar *massBck_p5   = Workspace->var("mass_massBck_p5");
    RooRealVar *massBck_p6   = Workspace->var("mass_massBck_p6");
    RooRealVar *massBck_p7   = Workspace->var("mass_massBck_p7");

    RooRealVar *RefittedBsPimassErr   = Workspace->var("RefittedBsPimassErr"); ///!!!!!
    
    BspiSignalEvents->setConstant(kTRUE);    
    BspiPDF->fitTo(*BspiData);
    BspiSignalEvents->setConstant(kFALSE);
    

    if(bckType == "rooAtlas")
    { 
        mass_massBck_a->setConstant(kTRUE);
        mass_massBck_norm->setConstant(kTRUE);
        mass_massBck_p1->setConstant(kTRUE);
        mass_massBck_p2->setConstant(kTRUE);
        mass_massBck_p3->setConstant(kTRUE);
        mass_massBck_p4->setConstant(kTRUE);
    }
    else if (bckType == "chebychev(7)"){
        massBck_p1->setConstant(kTRUE);
        massBck_p2->setConstant(kTRUE);
        massBck_p3->setConstant(kTRUE);
        massBck_p4->setConstant(kTRUE);
        massBck_p5->setConstant(kTRUE);
        massBck_p6->setConstant(kTRUE);
        massBck_p7->setConstant(kTRUE);
    }
    
    Double_t nFit = BspiSignalEvents->getVal();

    ModelConfig Model("_Model", Workspace);
    if(usePCE){
        Model.SetObservables(RooArgSet(*BspiMass, *RefittedBsPimassErr));    
    }
    else{
        Model.SetObservables(RooArgSet(*BspiMass));
    }
    Model.SetParametersOfInterest(*BspiSignalEvents);
    Model.SetNuisanceParameters(RooArgSet(*BspiBackgroundEvents));
    
    Model.SetPdf(*BspiPDF);
    
    TCanvas C("C", "C", 800, 800);
    RooPlot *BspiMassPlot = BspiMass->frame(Title("BspiMass"));

    BspiData->plotOn(BspiMassPlot);
    BspiPDF->plotOn(BspiMassPlot, LineColor(kBlack));
    if(usePCE){
        BspiPDF->plotOn(BspiMassPlot, Components("condMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("condMassBackground"), LineColor(kRed));    
    }else{
        BspiPDF->plotOn(BspiMassPlot, Components("convMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("mass_massBck"), LineColor(kRed));    
    }
    BspiMassPlot->Draw();
    cout << "INFO: Printing Image2" <<endl;
    C.SaveAs(name+suffix+"Nx.png");

    ModelConfig *SBModel = Model.Clone();
    SBModel->SetName("SBModel");
    ModelConfig *BModel = Model.Clone();
    BModel->SetName("BModel");
    
    RooRealVar *PoI = dynamic_cast<RooRealVar *>(Model.GetParametersOfInterest()->first());
    PoI->setVal(0.1);
    SBModel->SetSnapshot(*PoI);
    PoI->setVal(0.0);
    BModel->SetSnapshot(*PoI);
    
    //AsymptoticCalculator::SetPrintLevel(-1);
    AsymptoticCalculator Calculator(*BspiData, *BModel, *SBModel);
    Calculator.SetOneSided(true);
    
    HypoTestInverter TestInverter(Calculator);
    TestInverter.SetConfidenceLevel(0.95);
    TestInverter.UseCLs(true);
    TestInverter.SetVerbose(false);
    
    TestInverter.SetFixedScan(21, 0.0, 800.0);
    HypoTestInverterResult *Result = TestInverter.GetInterval();
    
    Double_t Down2, Down, Mean, Up, Up2, Limit;
    Down2 = Result->GetExpectedUpperLimit(-2);
    Down = Result->GetExpectedUpperLimit(-1);
    Mean = Result->GetExpectedUpperLimit(0);
    Up = Result->GetExpectedUpperLimit(1);
    Up2 = Result->GetExpectedUpperLimit(2);
    Limit = Result->UpperLimit();
    
    cout << endl <<"LIMIT:"<<mean<<";"<< nFit << ";" << Down2 << ";" << Down << ";" << Mean << ";" << Up << ";" << Up2 << ";" << Limit<<";" << endl;
    cout << endl <<"RES:"<<usePCE<<";"<<massSyst<<";"<<gammaSyst<<";"<<typeBW<< ";"<< bckType<< ";"<<Limit<<";"<<endl;

    Workspace->Print();
    //--------------------------------------------------------

    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;

    return;
}

void massScanBsPiRun2(const TString &name = "results/BsPi2015and2016RD_massScan", const Double_t &BsPtInt = 10., const Double_t &mean = 5567.8, const TString typeBW = "Swave", const TString bckType = "rooAtlas", const Double_t &systSF = 0., const Bool_t &fitPunzi = false, Bool_t usePCE = false, Int_t massSyst = 0, Int_t gammaSyst = 0, TString year = "2016") {
    const time_t startTime = TTimeStamp().GetSec();
    SetAtlasStyle();
    checkDir(name);
    TString suffix = Form("_BpTgt%.0fGeV_mean%.2fMeV_systSF%.0f", BsPtInt, mean, systSF);

    const Double_t SF      = 1.07;
    const Double_t sigmaSF = 0.04;
    const TString cut      = Form("B_pT > %.0f && BsPiSubMass < 5900 && had_3pT > 1000 && had_4pT > 1000 && had_5pT > 500 && BsTau > 0.2 && Track5FromPV && bestChiBs && BsMass > %.0f && BsMass < %.0f", BsPtInt * 1000., signalBandBsMin, signalBandBsMax); 
    suffix += "_defaultCuts";


    cout <<"INFO: FitModel" <<endl;
    cout <<"\t\t Bck Type: "<<bckType <<endl;
    cout <<"\t\t Sig Type: "<<typeBW<<endl;
    cout <<"\t\t usePCE: "<<usePCE <<endl;
    cout <<"\t\t massSyst: "<<massSyst <<endl;
    cout <<"\t\t gammaSyst: "<<gammaSyst <<endl;

    suffix += "_"+typeBW;

    if(usePCE){
        cout << "ERROR: Per candidate fit for RUN2 not supported!" <<endl;
        return;
    }

    Observables *observables = new Observables("observables");
    observables->set(MASS, "BsPiSubMass",  "#font[52]{m}(#font[52]{B_{s}}^{0}#font[152]{#pi}^{#pm})", PDGBsMass+PDGpiMass, PDGBsMass+PDGpiMass + 300.);
    RooRealVar *BsPt = new RooRealVar("B_pT", "#font[52]{B_{s}}^{0} #font[52]{p}_{T}", 0., 15e4, "MeV"); // for epsRel
    observables->add(BsPt); // for epsRel

    Data *data15 = new Data("data15", "data15Tracks_nopv_v3.root", "PVTracksComb PVTracksComb_extra", observables, cut);
    Data *data16 = new Data("data16", "data16allYear_BsOnlyTree.root", "PVTracksComb PVTracksComb_extra", observables, cut);

    Data *data;
    if(year == "2015") data = data15;
    else if(year == "2016") data = data16;
    else if(year == "2015and2016"){
        data = data15;
        data->add(data16);
    }
    else {
        cout << "ERROR: Invalid input year!"<< endl;
        return;
    }
    data->SetName("data");

    // Mass window
    // ====================================
    // RelMassError function
    Double_t r0 = -7.76044e+3;
    Double_t r1 =  5.41293e+3;
    Double_t r2 =  1.40965e-1;
    Double_t r3 =  3.16440e-2;
    Double_t r4 = -1.78405e-6;
    Double_t calcMassErr = ( mean - 5366.6 ) * ( r0 * TMath::Landau( mean, r1, r2 ) + ROOT::Math::Chebyshev1( mean, r3, r4 ) );

    // BW(mean, gamma_D0) (x) gauss(0., calcMassErr); signal PDF shape only, no fitting
    ModelPDF *massWindowModel = new ModelPDF("massWindow", data);
    massWindowModel->setMassSignalPDF("massWindowSig", "relBW"+typeBW);
    massWindowModel->setMassResolutionPDF("massWindowRes", "gauss");
    massWindowModel->setPars("massWindowSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
    massWindowModel->setPars("massWindowSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0    
    massWindowModel->setPars("massWindowSig_massA", PDGBsMass, true);
    massWindowModel->setPars("massWindowSig_massB", PDGpiMass, true);
    massWindowModel->setPars("massWindowRes_mean", 0., true);
    massWindowModel->setPars("massWindowRes_sigma", ( SF + ( systSF * sigmaSF ) ) * calcMassErr, true);

    // BsPi mass range - signal = PDF_value > 0.05 max_PDF_value; max_PDF_value != PDF_value(@mean), I don't know why...
    Int_t nSteps = 3000;
    Double_t maxPdfVal = 0.;
    Double_t pdfVal = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        observables->get(MASS)->setVal( 5500. + (Double_t)i / 10.0 );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        maxPdfVal = ( pdfVal > maxPdfVal ? pdfVal : maxPdfVal );
    }
    Double_t min = 0.;
    Double_t max = 0.;
    Double_t val = 0.;
    for (Int_t i = 0; i < nSteps; i++) {
        val = 5500. + (Double_t)i / 10.0;
        observables->get(MASS)->setVal( val );
        pdfVal = massWindowModel->getTotalMassSignalPDF()->getVal();
        if ( min == 0. ) {
            if ( pdfVal / maxPdfVal >= 0.05 ) min = val;
        } else if ( max == 0. ) {
            if ( pdfVal / maxPdfVal <= 0.05 ) max = val;
        }
    }
    if ( min < observables->get(MASS)->getMin() || max > observables->get(MASS)->getMax() || min == 0. || max == 0. ) {
        cout << "INFO: Mass window ( " << min << " ; " << max << " ) MeV is out of the data range ( " << observables->get(MASS)->getMin() << " ; " << observables->get(MASS)->getMax() << " ) MeV." << endl;
        return;
    }

    // BsPi mass fit, fixed mean and gamma_D0
    TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*800, ( fitPunzi ? 2 : 1 ) * 600);

    ModelPDF *massModel;
    ModelPDF *massErrorModel;

    if(!usePCE){
        suffix += "_tripleGauss";
        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "tripleGauss");
        massModel->setMassBackgroundPDF("massBck", bckType);
        massModel->setPars("massSig_mean", mean+massSyst*sqrt(pow(D0meanErrStat,2)+pow( massSyst == 1 ? D0meanErrSystUp : D0meanErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_gamma", D0gamma+gammaSyst*sqrt(pow(D0gammaErrStat,2)+pow( gammaSyst == 1 ? D0gammaErrSystUp : D0gammaErrSystDown,2) ), true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_fraction1", 0.17, true);
        massModel->setPars("massRes_fraction2", 0.18, true);
        massModel->setPars("massRes_sigma1", 1.51*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma2", 6.48*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        massModel->setPars("massRes_sigma3", 3.06*(1+ (mean-5567.8)/(5650-5567.8)*0.19 ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    else{
        suffix += "_PCE";
        massErrorModel = new ModelPDF("massError", data);
        massErrorModel->setMassSignalErrorPDF("massErrorSig", "3Gamma");
        if ( fitPunzi ) {
            can1->Divide(1, 2);
            can1->cd(2);
            massErrorModel->setPars("massErrorSig_a1", 3.);
            massErrorModel->setPars("massErrorSig_a2", 1.);
            massErrorModel->setPars("massErrorSig_a3", 5.);
            massErrorModel->setPars("massErrorSig_b1", 1.);
            massErrorModel->setPars("massErrorSig_b2", 3.);
            massErrorModel->setPars("massErrorSig_b3", 2.);
            massErrorModel->setPars("massErrorSig_c1", 0., true);
            massErrorModel->setPars("massErrorSig_c2", .1);
            massErrorModel->setPars("massErrorSig_c3", 0., true);
            massErrorModel->fit();
            massErrorModel->printResults();
            massErrorModel->drawPlot(MASSERROR, 150, "Mass Error in All Data");
            //can1->SaveAs(name+suffix+".png");
        } else {
            if ( BsPtInt == 10. ) {
                massErrorModel->setPars("massErrorSig_a1", 3.5148, true);
                massErrorModel->setPars("massErrorSig_a2", 0.9040, true);
                massErrorModel->setPars("massErrorSig_a3", 4.2344, true);
                massErrorModel->setPars("massErrorSig_b1", 1.07368, true);
                massErrorModel->setPars("massErrorSig_b2", 3.42336, true);
                massErrorModel->setPars("massErrorSig_b3", 0.9777, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.1005, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.3345, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.6367, true);
            } else if ( BsPtInt == 15. ) {
                massErrorModel->setPars("massErrorSig_a1", 10.54, true);
                massErrorModel->setPars("massErrorSig_a2", 1.516, true);
                massErrorModel->setPars("massErrorSig_a3", 5.35, true);
                massErrorModel->setPars("massErrorSig_b1", 0.4313, true);
                massErrorModel->setPars("massErrorSig_b2", 2.229, true);
                massErrorModel->setPars("massErrorSig_b3", 2.405, true);
                massErrorModel->setPars("massErrorSig_c1", 0., true);
                massErrorModel->setPars("massErrorSig_c2", 0.0372, true);
                massErrorModel->setPars("massErrorSig_c3", 0., true);
                massErrorModel->setPars("massErrorSig_fraction", 0.1758, true);
                massErrorModel->setPars("massErrorSig_fraction2", 0.9684, true);
            } else {
                cout << "ERROR: Missing Punzi parameters." << endl;
                return;
            }
        }

        massModel = new ModelPDF("mass", data, true);
        massModel->setMassSignalPDF("massSig", "relBW"+typeBW);
        massModel->setMassResolutionPDF("massRes", "gaussPCE");
        massModel->setMassSignalErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massModel->setMassBackgroundPDF("massBck", bckType ); 
        massModel->setPars("massSig_mean", mean, true); // D0
        massModel->setPars("massSig_gamma", 21.9, true); // D0
        massModel->setPars("massSig_massA", PDGBsMass, true);
        massModel->setPars("massSig_massB", PDGpiMass, true);
        massModel->setPars("massRes_mean", 0., true);
        massModel->setPars("massRes_scaleFactor", ( SF + ( systSF * sigmaSF ) ), true);
        if(bckType == "rooAtlas")
        {
            massModel->setPars("massBck_mass0", PDGBsMass+PDGpiMass, true);
            massModel->setPars("massBck_a", 0.6);
            massModel->setPars("massBck_norm", 180., 1e-2, 1e4);
            massModel->setPars("massBck_p0", 0., true);
            massModel->setPars("massBck_p3", -0.1);
            massModel->setPars("massBck_p4", 0.1);
        }
        else if (bckType == "chebychev(7)"){
            massModel->setPars("massBck_p1", 0.1);
            massModel->setPars("massBck_p2", 0.1);
            massModel->setPars("massBck_p3", 0.1);
            massModel->setPars("massBck_p4", 0.1);
            massModel->setPars("massBck_p5", 0.1);
            massModel->setPars("massBck_p6", 0.1);
            massModel->setPars("massBck_p7", 0.1);
        }
        
        massModel->setPars(massErrorModel, true);
        massModel->setPars("nSig", 0, -5000, data->getDataset()->numEntries());
        massModel->setPars("nBck", data->getDataset()->numEntries() - 20, 0, data->getDataset()->numEntries() + 5000);
    }
    delete can1;

    cout << "\n============================================================================================================\n" << endl;
    //--------------------------------------------------------
    RooWorkspace *Workspace = new RooWorkspace("Workspace","workspace") ;
    //data->saveToWorkspace(Workspace);
    massModel->saveToWorkspace(Workspace);
    Workspace->Print();
    
    RooAbsData *BspiData = Workspace->data("data");
    RooRealVar *BspiMass = Workspace->var("BsPiSubMass");
    RooRealVar *BspiSignalEvents = Workspace->var("mass_nSig");
    RooRealVar *BspiBackgroundEvents = Workspace->var("mass_nBck");
    RooAbsPdf *BspiPDF = Workspace->pdf("totalModel");


    RooRealVar *mass_massBck_a    = Workspace->var("mass_massBck_a");
    RooRealVar *mass_massBck_norm = Workspace->var("mass_massBck_norm");
    RooRealVar *mass_massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *mass_massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *mass_massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *mass_massBck_p4   = Workspace->var("mass_massBck_p4");

    RooRealVar *massBck_p1   = Workspace->var("mass_massBck_p1");
    RooRealVar *massBck_p2   = Workspace->var("mass_massBck_p2");
    RooRealVar *massBck_p3   = Workspace->var("mass_massBck_p3");
    RooRealVar *massBck_p4   = Workspace->var("mass_massBck_p4");
    RooRealVar *massBck_p5   = Workspace->var("mass_massBck_p5");
    RooRealVar *massBck_p6   = Workspace->var("mass_massBck_p6");
    RooRealVar *massBck_p7   = Workspace->var("mass_massBck_p7");

    
    BspiSignalEvents->setConstant(kTRUE);    
    BspiPDF->fitTo(*BspiData);
    BspiSignalEvents->setConstant(kFALSE);
    

    if(bckType == "rooAtlas")
    { 
        mass_massBck_a->setConstant(kTRUE);
        mass_massBck_norm->setConstant(kTRUE);
        mass_massBck_p1->setConstant(kTRUE);
        mass_massBck_p2->setConstant(kTRUE);
        mass_massBck_p3->setConstant(kTRUE);
        mass_massBck_p4->setConstant(kTRUE);
    }
    else if (bckType == "chebychev(7)"){
        massBck_p1->setConstant(kTRUE);
        massBck_p2->setConstant(kTRUE);
        massBck_p3->setConstant(kTRUE);
        massBck_p4->setConstant(kTRUE);
        massBck_p5->setConstant(kTRUE);
        massBck_p6->setConstant(kTRUE);
        massBck_p7->setConstant(kTRUE);
    }
    
    Double_t nFit = BspiSignalEvents->getVal();

    ModelConfig Model("_Model", Workspace);
    if(usePCE){
        Model.SetObservables(RooArgSet(*BspiMass));    
    }
    else{
        Model.SetObservables(RooArgSet(*BspiMass));
    }
    Model.SetParametersOfInterest(*BspiSignalEvents);
    Model.SetNuisanceParameters(RooArgSet(*BspiBackgroundEvents));
    
    Model.SetPdf(*BspiPDF);
    
    TCanvas C("C", "C", 800, 800);
    RooPlot *BspiMassPlot = BspiMass->frame(Title("BspiMass"));

    BspiData->plotOn(BspiMassPlot);
    BspiPDF->plotOn(BspiMassPlot, LineColor(kBlack));
    if(usePCE){
        BspiPDF->plotOn(BspiMassPlot, Components("condMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("condMassBackground"), LineColor(kRed));    
    }else{
        BspiPDF->plotOn(BspiMassPlot, Components("convMassSignal"), LineColor(kGreen));
        BspiPDF->plotOn(BspiMassPlot, Components("mass_massBck"), LineColor(kRed));    
    }
    BspiMassPlot->Draw();
    cout << "INFO: Printing Image2" <<endl;
    C.SaveAs(name+suffix+"Nx.png");

    ModelConfig *SBModel = Model.Clone();
    SBModel->SetName("SBModel");
    ModelConfig *BModel = Model.Clone();
    BModel->SetName("BModel");
    
    RooRealVar *PoI = dynamic_cast<RooRealVar *>(Model.GetParametersOfInterest()->first());
    PoI->setVal(0.1);
    SBModel->SetSnapshot(*PoI);
    PoI->setVal(0.0);
    BModel->SetSnapshot(*PoI);
    
    //AsymptoticCalculator::SetPrintLevel(-1);
    AsymptoticCalculator Calculator(*BspiData, *BModel, *SBModel);
    Calculator.SetOneSided(true);
    
    HypoTestInverter TestInverter(Calculator);
    TestInverter.SetConfidenceLevel(0.95);
    TestInverter.UseCLs(true);
    TestInverter.SetVerbose(false);
    
    TestInverter.SetFixedScan(21, 0.0, 800.0);
    HypoTestInverterResult *Result = TestInverter.GetInterval();
    
    Double_t Down2, Down, Mean, Up, Up2, Limit;
    Down2 = Result->GetExpectedUpperLimit(-2);
    Down = Result->GetExpectedUpperLimit(-1);
    Mean = Result->GetExpectedUpperLimit(0);
    Up = Result->GetExpectedUpperLimit(1);
    Up2 = Result->GetExpectedUpperLimit(2);
    Limit = Result->UpperLimit();
    
    cout << endl <<"LIMIT:"<<mean<<";"<< nFit << ";" << Down2 << ";" << Down << ";" << Mean << ";" << Up << ";" << Up2 << ";" << Limit<<";" << endl;
    cout << endl <<"RES:"<<usePCE<<";"<<massSyst<<";"<<gammaSyst<<";"<<typeBW<< ";"<< bckType<< ";"<<Limit<<";"<<endl;

    Workspace->Print();
    //--------------------------------------------------------

    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;

    return;
}

void massScanBsPiNxFinal(Double_t massPiont = 5567.8) {
//    const TString typeBW = "Swave";
     const TString typeBW = "Pwave";
//    const TString bckType = "rooAtlas";
    const TString bckType = "chebychev(7)";
    
    //massPiont = 5650; /// test systematics effect
    Double_t ptCut = 10;
    Bool_t fitPunzi = false;
    Bool_t usePCE = false;
    Int_t systSF = 0;
    Int_t massSyst = 1;
    Int_t gammaSyst = 1;
    TString year = "2016";
    
    if( (year == "2011") || (year == "2012") || (year == "2011and2012")){
        massScanBsPiRun1("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, massSyst, gammaSyst, year);
    }
    else if( (year == "2015") || (year == "2016") || (year == "2015and2016")){
        massScanBsPiRun2("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, massSyst, gammaSyst, year);
    }
    //massScanBsPiRun1("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, massSyst, gammaSyst, year);
    //massScanBsPiRun2("results/BsPi"+year+"RD_massScan", ptCut, massPiont, typeBW, bckType, systSF, fitPunzi, usePCE, massSyst, gammaSyst, year);
    
    /*for(auto iMassSyst = -1; iMassSyst <= 1;iMassSyst++ ){
        for(auto iGammaSyst = -1; iGammaSyst <= 1;iGammaSyst++ ){
            for (auto iTypeBW : {"Swave", "Pwave"}){
                for (auto iBckType : { "rooAtlas" ,"chebychev(7)"  }){
                    massScanBsPiRun1("results/BsPi2011and2012RD_massScan", ptCut, massPiont, iTypeBW, iBckType, systSF, fitPunzi, usePCE, iMassSyst, iGammaSyst, year);
                }
            }               
        }
    }*/
    return;
}
