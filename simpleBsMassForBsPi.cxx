#include "BFitCore/IncludeAllSources.h"

const Double_t PDGBsMass = 5366.8; // (5366.82 +/- 0.22) MeV
const Double_t PDGBsTau  = 1.41; // (1.414 +/- 0.007) ps (dominated by BsL) OR (1.479 +/- 0.012) ps (effective lifetime from single exponential fits, http://www.slac.stanford.edu/xorg/hfag/osc/summer_2016/)

void simpleBsFitRD(const TString &name = "results/Bs2011and2012RD", const Double_t &BsPtInt = 10., const TString &massGausses = "double") {
    const time_t startTime = TTimeStamp().GetSec();
    SetAtlasStyle();
    checkDir(name);
    TString suffix = Form("_BpTgt%.0fGeV_%sGauss", BsPtInt, massGausses.Data());

    const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.2", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.10", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.15", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.25", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.30", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.35", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.4", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.5", BsPtInt * 1000.); 
    //const TString cut = Form("B_pT > %.0f && had_3pT > 1000 && had_4pT > 1000 && BsTau > 0.6", BsPtInt * 1000.); 
    suffix += "_defaultCuts";

    Observables *observables = new Observables("observables");
    observables->set(MASS, "BsMass", "#font[52]{m}(#font[52]{B_{s}^{0}})", 5150., 5650.);

    Data *data11 = new Data("data11", "data11cascadeBsPi.bsConstraint.NoBestChi.v10.root", "BsOnly", observables, cut);
    Data *data12 = new Data("data12", "data12cascadeBsPi.bsConstraint.NoBestChi.v10.root", "BsOnly", observables, cut);
    Data *data = data11;
    data->add(data12);

    ModelPDF *massTimeModel = new ModelPDF("massTime", data);
    massTimeModel->setMassSignalPDF("massSig", massGausses+"Gauss");
    massTimeModel->setMassBackgroundPDF("massBck", "exponentialAndConstant");
    massTimeModel->setPars("massSig_mean", PDGBsMass, PDGBsMass-50., PDGBsMass+50.);
    massTimeModel->setPars("massSig_fraction", .2);
    massTimeModel->setPars("massSig_fraction1", .2);
    massTimeModel->setPars("massSig_fraction2", .4);
    massTimeModel->setPars("massSig_sigma1", 5.);
    massTimeModel->setPars("massSig_sigma2", 15.);
    massTimeModel->setPars("massSig_sigma3", 25.);
    massTimeModel->setPars("massBck_slope", -0.002);
    massTimeModel->setPars("massBck_fraction", .5);
    massTimeModel->setPars("sigFrac", .2);
    //massTimeModel->setPars("sigFrac", .8);
    massTimeModel->fit();

    TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*800, 1*600);
    massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
    ATLASLabel(0.2, 0.77, "Internal", 1, 0.045);
    myText(0.2, 0.72, 1, "#sqrt{s}=7 TeV, 4.9 fb^{-1}", 0.04);
    myText(0.2, 0.67, 1, "#sqrt{s}=8 TeV, 19.5 fb^{-1}", 0.04);
    can1->Update();

    can1->SaveAs(name+suffix+".png");
    can1->SaveAs(name+suffix+".eps");
    can1->SaveAs(name+suffix+".pdf");
    can1->SaveAs(name+suffix+".C");
    delete can1;
    
    massTimeModel->printModel();
    massTimeModel->printResults();
    RooRealVar* nSig =  massTimeModel->getNSigInRange(5346,5386);
    cout << "RES:" << nSig->getVal() << "+/-" << nSig->getError() <<endl;
    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;
}

void simpleBsMassForBsPi() {
    //simpleBsFitRD("results/Bs2011and2012RDcontrol", 10., "double");
    simpleBsFitRD("results/Bs2011and2012RD", 10., "double");
    //simpleBsFitRD("results/Bs2011and2012RD", 10., "triple");
    //simpleBsFitRD("results/Bs2011and2012RD", 15., "double");
    //simpleBsFitRD("results/Bs2011and2012RD", 15., "triple");
    return;
}
