#include "Data.h"

Data::Data(const TString &name, const TString &fileName, const TString &treeName, Observables *observables, const TString &cuts) : m_name(name),m_observables(observables),m_vars(observables->getVars()) {
    m_filesLocations.push_back("");
    m_filesLocations.push_back("/data0/jakoubek/RUN1/BsJpsiphi/");
    m_filesLocations.push_back("/data0/jakoubek/RUN2/BsJpsiphi/");
    m_filesLocations.push_back("/data0/jakoubek/RUN1/BsPi/");
    m_filesLocations.push_back("/data0/jakoubek/RUN2/BpmJpsiKpm/");
    m_filesLocations.push_back("/afs/cern.ch/work/j/jakoubek/BsPi/");
    m_filesLocations.push_back("/afs/cern.ch/user/a/abarton/WorkSpace/public/BsPi/");
    m_filesLocations.push_back("/afs/cern.ch/user/a/abarton/WorkSpace/public/JpsiPhi/");
    m_filesLocations.push_back("/afs/cern.ch/user/a/abarton/WorkSpace/public/");
    m_filesLocations.push_back("/data0/novotnyr/BsPi/BsFitTest/BFitNewNew/b-phys/BFit/");
    m_filesLocations.push_back("/data0/novotnyr/data/BsPi/");

    for ( UInt_t i = 0; i < m_filesLocations.size(); i++ ) {
        if ( rootFileExists(m_filesLocations.at(i) + fileName) ) {
            m_file = new TFile(m_filesLocations.at(i) + fileName, "READ");
            cout << "INFO: Opening file " << m_filesLocations.at(i) << fileName << endl;
            break;
        }
    }
    if ( m_file == nullptr ) {
        cout << "ERROR: Can't open a file " << fileName << endl;
        gApplication->Terminate();
    }

    vector<TString> trees = splitString(treeName);

    if ( !( m_tree = (TTree*)m_file->Get(trees.at(0)) ) ) {
        cout << "ERROR: Can't import a tree " << trees.at(0) << endl;
        gApplication->Terminate();
    }
    if ( trees.size() > 1 ) {
        for ( UInt_t i = 1; i < trees.size(); i++ ) {
            if ( !( m_tree->AddFriend( (TTree*)m_file->Get(trees.at(i)) ) ) ) {
                cout << "ERROR: Can't add a tree " << trees.at(i) << endl;
                gApplication->Terminate();
            }
        }
    }
    cout << "INFO: Tree(s) ";
    for ( UInt_t i = 0; i < trees.size(); i++ ) {
        cout << trees.at(i) << " ";
    }
    cout << "imported" << endl;

    TIterator *vars_it = m_vars.createIterator();
    RooRealVar *var;

    TString newCuts = cuts;
    for ( var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
        TString varName = Form("%s", var->GetName());
        newCuts += " && " + varName + " > ";
        newCuts += var->getMin();
        newCuts += " && " + varName + " < ";
        newCuts += var->getMax();
    }

    cout << "INFO: Cuts = " << newCuts << endl;

    Long64_t nEvents = m_tree->Draw("1", newCuts, "goff");
    m_tree->SetEstimate(nEvents);

    TString varNames = "";
    TString varNamesDelim = "";
    vars_it->Reset();
    for ( var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
        if ( m_tree->GetListOfBranches()->FindObject(var->GetName()) ) {
            varNames += varNamesDelim + var->GetName();
        } else {
            cout << "WARNING: Variable " << var->GetName() << " is not present in the file (trees) " << fileName << " (" << treeName << ")" << endl;
            varNames += varNamesDelim + "1";
        }
        varNamesDelim = ":";
    }
    m_tree->Draw(varNames, newCuts, "para goff");

    m_dataset = new RooDataSet(m_name, newCuts, m_vars);
    for ( Long64_t i = 0; i < nEvents; i++ ) {
        vars_it->Reset();
        Int_t j = 0;
        for ( var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
            var->setVal(m_tree->GetVal(j)[i]);
            j++;
        }
        m_dataset->add(m_vars);
    }
}

Data::~Data() {
    delete m_dataset;
    m_file->Close();
    delete m_file;
    cout << "INFO: Data deleted." << endl;
}

Data* Data::reduce(const TString &name, const TString &cuts) {
    RooDataSet *newDataset = (RooDataSet*)m_dataset->reduce(cuts);
    newDataset->SetName(name);
    newDataset->SetTitle( (TString)newDataset->GetTitle() + " && " + cuts );
    Data *newData = new Data(name, newDataset, m_observables);
    return newData;
}

Data* Data::randomSample(const TString &name, const Long64_t &n) {
    Long64_t nOrig = m_dataset->numEntries();
    TString title = m_dataset->GetTitle();
    title += " Random sample ( ";
    title += n;
    title += " / ";
    title += nOrig;
    title += " )";
    RooDataSet *newDataset = new RooDataSet(m_name + "RandomSample", title, m_vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < n; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(m_dataset->get( j )) );
    }
    cout << "INFO: Created random sample (" << n << " from " << nOrig << ")" << endl;
    Data *newData = new Data(name, newDataset, m_observables);
    return newData;
}

Bool_t Data::isEmpty(void) {
    if ( m_file == nullptr || m_tree == nullptr || m_dataset == nullptr || m_dataset->numEntries() == 0 ) return true;
    else return false;
}

void Data::add(Data *data) {
    m_dataset->append(*(data->getDataset()));
}

void Data::SetName(const char *name){
    m_dataset->SetName(name);
}