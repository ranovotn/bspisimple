#ifndef OBSERVABLES
#define OBSERVABLES

#include "RooRealVar.h"
#include "RooArgSet.h"

enum Observable {
    MASS,
    MASSERROR,
    TIME,
    TIMEERROR,
    PT
};

class Observables {
public:
    Observables(const TString &name) : m_name(name) {}
    void set(const Observable &var, const TString &varName, const TString &varTitle, const Double_t &varMin, const Double_t &varMax);
    void set(const Observable &var, RooRealVar *rooVar);
    void add(RooRealVar *rooVar);
    RooRealVar* get(const Observable &var);
    RooArgSet getVars(void) { return m_vars; }
private:
    TString m_name;
    RooArgSet m_vars;
    RooRealVar *m_mass      = nullptr;
    RooRealVar *m_massError = nullptr;
    RooRealVar *m_time      = nullptr;
    RooRealVar *m_timeError = nullptr;
    RooRealVar *m_pt        = nullptr;
};

#endif
