#ifndef DATA
#define DATA

#include "TSystem.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TTree.h"
#include "RooDataSet.h"

#include "Utils.h"

class Data {
public:
    Data(const TString &name, const TString &fileName, const TString &treeName, Observables *observables, const TString &cuts);
    Data(const TString &name, RooDataSet *dataset, Observables *observables) : m_dataset(dataset),m_name(name),m_observables(observables) {}
    ~Data();
    Data* reduce(const TString &name, const TString &cuts);
    Data* randomSample(const TString &name, const Long64_t &n);
    void add(Data *data);
    void print(void) { m_dataset->Print(); }
    Bool_t isEmpty(void);
    RooDataSet* getDataset(void) { return m_dataset; }
    Observables* getObservables(void) { return m_observables; }
    void SetName(const char *name);
private:
    TFile *m_file = nullptr;
    TTree *m_tree = nullptr;
    RooDataSet *m_dataset = nullptr;
    Observables *m_observables = nullptr;
    RooArgSet m_vars = m_observables->getVars();
    TString m_name;
    vector<TString> m_filesLocations;
};

#endif
