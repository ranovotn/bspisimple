#include "Observables.h"

void Observables::set(const Observable &var, const TString &varName, const TString &varTitle, const Double_t &varMin, const Double_t &varMax) {
    switch( var ) {
        case MASS :
            m_mass = new RooRealVar(varName, varTitle, varMin, varMax, "MeV");
            m_vars.add(*m_mass);
            break;
        case MASSERROR :
            m_massError = new RooRealVar(varName, varTitle, varMin, varMax, "MeV");
            m_vars.add(*m_massError);
            break;
        case TIME :
            m_time = new RooRealVar(varName, varTitle, varMin, varMax, "ps");
            m_vars.add(*m_time);
            break;
        case TIMEERROR :
            m_timeError = new RooRealVar(varName, varTitle, varMin, varMax, "ps");
            m_vars.add(*m_timeError);
            break;
        case PT :
            m_pt = new RooRealVar(varName, varTitle, varMin, varMax, "MeV"); // "B_s p_T"
            m_vars.add(*m_pt);
            break;
    }
}

void Observables::set(const Observable &var, RooRealVar *rooVar) {
    switch( var ) {
        case MASS :
            m_mass = rooVar;
            m_vars.add(*m_mass);
            break;
        case MASSERROR :
            m_massError = rooVar;
            m_vars.add(*m_massError);
            break;
        case TIME :
            m_time = rooVar;
            m_vars.add(*m_time);
            break;
        case TIMEERROR :
            m_timeError = rooVar;
            m_vars.add(*m_timeError);
            break;
        case PT :
            m_pt = rooVar;
            m_vars.add(*m_pt);
            break;
    }
}

void Observables::add(RooRealVar *rooVar) {
    m_vars.add(*rooVar);
}

RooRealVar* Observables::get(const Observable &var) {
    switch( var ) {
        case MASS :
            return m_mass;
            break;
        case MASSERROR :
            return m_massError;
            break;
        case TIME :
            return m_time;
            break;
        case TIMEERROR :
            return m_timeError;
            break;
        case PT :
            return m_pt;
            break;
    }
}
