#include "ModelPDF.h"

void ModelPDF::setMassSignalPDF(const TString &name, const TString &pdf) {
    if ( pdf == "gauss" ) {
        RooRealVar *mean      = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma     = new RooRealVar (m_name+"_"+name+"_sigma", "Sigma of Gaussian", 1e-9, RooNumber::infinity());
        m_massSignalCorePDF   = new RooGaussian(m_name+"_"+name, "Mass Signal Gaussian", *m_observables->get(MASS), *mean, *sigma);
    } else if ( pdf == "doubleGauss" ) {
        RooRealVar *mean     = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1   = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2   = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar (m_name+"_"+name+"_fraction", "Fraction of DoubleGaussian", 0., 1.);
        RooGaussian *gauss1  = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2  = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        m_massSignalCorePDF  = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction);
    } else if ( pdf == "tripleGauss" ) {
        RooRealVar *mean      = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1    = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2    = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma3    = new RooRealVar (m_name+"_"+name+"_sigma3", "Sigma3 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction1 = new RooRealVar (m_name+"_"+name+"_fraction1", "Fraction1 of TripleGaussian", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar (m_name+"_"+name+"_fraction2", "Fraction2 of TripleGaussian", 0., 1.);
        RooGaussian *gauss1   = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2   = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        RooGaussian *gauss3   = new RooGaussian(m_name+"_"+name+"_gauss3", "Gaussian 3", *m_observables->get(MASS), *mean, *sigma3);
        m_massSignalCorePDF   = new RooAddPdf  (m_name+"_"+name, "Mass Signal TripleGaussian", RooArgList(*gauss1, *gauss2, *gauss3), RooArgList(*fraction1, *fraction2));
    } else if ( pdf == "gaussPCE" ) {
        RooRealVar *mean        = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scaleFactor = new RooRealVar (m_name+"_"+name+"_scaleFactor", "SF of Gaussian", 1., 0.1, 10.);
        RooProduct *sigma       = new RooProduct (m_name+"_"+name+"_sigma", "massErr * Scale Factor", RooArgList(*m_observables->get(MASSERROR), *scaleFactor));
        m_massSignalCorePDF     = new RooGaussian(m_name+"_"+name, "Mass Signal Gaussian with per Candidate Error", *m_observables->get(MASS), *mean, *sigma);
        m_conditionalObservables.add(*m_observables->get(MASSERROR));
        m_useProjWDataMass = true;
    } else if ( pdf == "relBWSwave" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        RooRealVar *type    = new RooRealVar(m_name+"_"+name+"_type", "Type of RelBW", 0);
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name, "Relativistic Breit-Wigner (S-wave)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, *type);
    } else if ( pdf == "relBWPwave" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        RooRealVar *type    = new RooRealVar(m_name+"_"+name+"_type", "Type of RelBW", 1);
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name, "Relativistic Breit-Wigner (P-wave)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, *type);
    } else if ( pdf == "relBWPythiaCorrected" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        RooRealVar *type    = new RooRealVar(m_name+"_"+name+"_type", "Type of RelBW", -1);
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name+"_relBW", "Relativistic Breit-Wigner (Pythia corrected Gamma)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, *type);
    } else if ( pdf == "relBWPythiaOrig" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        RooRealVar *type    = new RooRealVar(m_name+"_"+name+"_type", "Type of RelBW", -2);
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name+"_relBW", "Relativistic Breit-Wigner (Pythia original Gamma)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, *type);
    } else if ( pdf == "relBWStaticGamma" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", 1e-9, RooNumber::infinity()); // to avoid RooAbsReal::logEvalError
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 1e-9, RooNumber::infinity()); // to avoid RooAbsReal::logEvalError
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        RooRealVar *type    = new RooRealVar(m_name+"_"+name+"_type", "Type of RelBW", -9);
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name+"_relBW", "Relativistic Breit-Wigner (static Gamma)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, *type);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massSignal[0] = name;
    m_massSignal[1] = pdf;
}

void ModelPDF::setMassResolutionPDF(const TString &name, const TString &pdf) {
    if ( pdf == "gauss" ) {
        RooRealVar *mean      = new RooRealVar   (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma     = new RooRealVar   (m_name+"_"+name+"_sigma", "Sigma of Gaussian", 1e-9, RooNumber::infinity());
        m_massResolutionPDF   = new RooGaussModel(m_name+"_"+name, "Mass Signal Gaussian", *m_observables->get(MASS), *mean, *sigma);
    } else if ( pdf == "doubleGauss" ) {
        RooRealVar *mean     = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1   = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2   = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar (m_name+"_"+name+"_fraction", "Fraction of DoubleGaussian", 0., 1.);
        RooGaussian *gauss1  = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2  = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        m_massResolutionPDF  = new RooAddModel(m_name+"_"+name, "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction);
    } else if ( pdf == "tripleGauss" ) {
        RooRealVar *mean      = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1    = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2    = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma3    = new RooRealVar (m_name+"_"+name+"_sigma3", "Sigma3 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction1 = new RooRealVar (m_name+"_"+name+"_fraction1", "Fraction1 of TripleGaussian", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar (m_name+"_"+name+"_fraction2", "Fraction2 of TripleGaussian", 0., 1.);
        RooGaussian *gauss1   = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2   = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        RooGaussian *gauss3   = new RooGaussian(m_name+"_"+name+"_gauss3", "Gaussian 3", *m_observables->get(MASS), *mean, *sigma3);
        m_massResolutionPDF   = new RooAddModel(m_name+"_"+name, "Mass Signal TripleGaussian", RooArgList(*gauss1, *gauss2, *gauss3), RooArgList(*fraction1, *fraction2));
    } else if ( pdf == "gaussPCE" ) {
        RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Mass Resolution", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Mass Error", 1., 1e-9, RooNumber::infinity());
        m_massResolutionPDF     = new RooGaussModel(m_name+"_"+name, "Mass Resolution Gaussian (for both Sig and Bck)", *m_observables->get(MASS), *mean, *scaleFactor, *m_observables->get(MASSERROR));
        m_conditionalObservables.add(*m_observables->get(MASSERROR));
        m_useProjWDataMass = true;
    } else if ( pdf == "delta" ) {
        m_massResolutionPDF = new RooTruthModel(m_name+"_"+name, "Mass Resolution Delta (for both Sig and Bck)", *m_observables->get(MASS));
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massResolution[0] = name;
    m_massResolution[1] = pdf;
}

void ModelPDF::setMassBackgroundPDF(const TString &name, const TString &pdf) {
    if ( pdf == "exponentialAndConstant" ) {
        RooRealVar *slope           = new RooRealVar    (m_name+"_"+name+"_slope", "Slope of Exponential", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction        = new RooRealVar    (m_name+"_"+name+"_fraction", "Fraction of Exponential", 0., 1.);
        RooPolynomial *constant     = new RooPolynomial (m_name+"_"+name+"_constant", "Constant", *m_observables->get(MASS));
        RooExponential *exponential = new RooExponential(m_name+"_"+name+"_exponential", "Exponential", *m_observables->get(MASS), *slope);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Exponential+Constant", RooArgList(*exponential, *constant), *fraction);
    } else if ( pdf == "linear" ) {
        RooRealVar *shape           = new RooRealVar    (m_name+"_"+name+"_shape", "Shape of Linear", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF         = new RooPolynomial (m_name+"_"+name, "Mass Background Linear", *m_observables->get(MASS), *shape);
    } else if ( pdf == "linearAndTanh" ) {
        RooRealVar *shape           = new RooRealVar    (m_name+"_"+name+"_shape", "Shape of Linear", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scale           = new RooRealVar    (m_name+"_"+name+"_scale", "Scale of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *offset          = new RooRealVar    (m_name+"_"+name+"_offset", "Offset of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction        = new RooRealVar    (m_name+"_"+name+"_fraction", "Fraction of Linear", 0., 1.);
        RooPolynomial *linear       = new RooPolynomial (m_name+"_"+name+"_linear", "Linear", *m_observables->get(MASS), *shape);
        RooTanhPdf *tanh            = new RooTanhPdf    (m_name+"_"+name+"_tanh", "Tanh", *m_observables->get(MASS), *scale, *offset);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Linear+Tanh", RooArgList(*linear, *tanh), *fraction);
    } else if ( pdf == "exponentialAndTanh" ) {
        RooRealVar *slope           = new RooRealVar    (m_name+"_"+name+"_slope", "Slope of Exponential", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scale           = new RooRealVar    (m_name+"_"+name+"_scale", "Scale of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *offset          = new RooRealVar    (m_name+"_"+name+"_offset", "Offset of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction1       = new RooRealVar    (m_name+"_"+name+"_fraction1", "Fraction of Exponential", 0., 1.);
        RooRealVar *fraction2       = new RooRealVar    (m_name+"_"+name+"_fraction2", "Fraction to Tanh", 0., 1.);
        RooExponential *exponential = new RooExponential(m_name+"_"+name+"_exponential", "Exponential", *m_observables->get(MASS), *slope);
        RooPolynomial *constant     = new RooPolynomial (m_name+"_"+name+"_constant", "Constant", *m_observables->get(MASS));
        RooTanhPdf *tanh            = new RooTanhPdf    (m_name+"_"+name+"_tanh", "Tanh", *m_observables->get(MASS), *scale, *offset);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Linear+Tanh", RooArgList(*exponential, *constant, *tanh), RooArgList(*fraction1,*fraction2));
    } else if ( pdf == "expPol" ) {
        RooRealVar *mass0   = new RooRealVar(m_name+"_"+name+"_mass0", "Mass0 of ExpPol", 0., RooNumber::infinity());
        RooRealVar *norm    = new RooRealVar(m_name+"_"+name+"_norm", "Norm of ExpPol", 0., RooNumber::infinity());
        RooRealVar *a       = new RooRealVar(m_name+"_"+name+"_a", "A of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p1      = new RooRealVar(m_name+"_"+name+"_p1", "P1 of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p2      = new RooRealVar(m_name+"_"+name+"_p2", "P2 of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p3      = new RooRealVar(m_name+"_"+name+"_p3", "P3 of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF = new RooExpPol (m_name+"_"+name, "ExpPol", *m_observables->get(MASS), *mass0, *norm, *a, *p1, *p2, *p3);
    } else if ( pdf == "rooAtlas" ) {
        RooRealVar *mass0   = new RooRealVar(m_name+"_"+name+"_mass0", "Mass0 of Atlas", 0., RooNumber::infinity());
        RooRealVar *norm    = new RooRealVar(m_name+"_"+name+"_norm", "Norm of Atlas", 0., RooNumber::infinity());
        RooRealVar *a       = new RooRealVar(m_name+"_"+name+"_a", "A of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p0      = new RooRealVar(m_name+"_"+name+"_p0", "P0 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p1      = new RooRealVar(m_name+"_"+name+"_p1", "P1 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p2      = new RooRealVar(m_name+"_"+name+"_p2", "P2 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p3      = new RooRealVar(m_name+"_"+name+"_p3", "P3 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p4      = new RooRealVar(m_name+"_"+name+"_p4", "P4 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF = new RooATLAS  (m_name+"_"+name, "RooAtlas", *m_observables->get(MASS), *mass0, *norm, *a, *p4, *p3, *p2, *p1, *p0);
    } else if ( pdf.Contains("chebychev") ) {
        TString s1(pdf);
        TPRegexp("chebychev\\((\\d{1})\\)").Substitute(s1, "$1");
        string s2(s1);
        Int_t order = atoi(s2.c_str());
        if ( order > 7) {
            cout << "ERROR: Chebychev > 7" << endl;
            gApplication->Terminate();
        }
        RooRealVar *p[7];
        RooArgList pList;
        for ( Int_t i = 0; i < order; i++ ) {
            TString iStr = "";
            iStr += (i+1);
            p[i] = new RooRealVar(m_name+"_"+name+"_p"+iStr, "P"+iStr+" of Chebychev", -RooNumber::infinity(), RooNumber::infinity());
            pList.add(*p[i]);
        }
        m_massBackgroundPDF = new RooChebychev(m_name+"_"+name, "Chebychev Polynomial ("+s1+")", *m_observables->get(MASS), pList);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massBackground[0] = name;
    m_massBackground[1] = pdf;
}

void ModelPDF::setTimeSignalPDF(const TString &name, const TString &pdf) {
    if ( pdf == "singleExponential" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name, "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tau = new RooRealVar(m_name+"_"+name+"_tau", "Lifetime Signal Tau", -RooNumber::infinity(), RooNumber::infinity());
        m_timeSignalPDF = new RooDecay  (m_name+"_"+name+"_decay", "Lifetime Signal Decay", *m_observables->get(TIME), *tau, *m_timeResolutionPDF, RooDecay::SingleSided);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeSignal[0] = name;
    m_timeSignal[1] = pdf;
}

void ModelPDF::setTimeBackgroundPDF(const TString &name, const TString &pdf) {
    if ( pdf == "doubleExponential" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name, "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tauPos1              = new RooRealVar(m_name+"_"+name+"_tauPos1", "Bck Tau Pos 1", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauPos2              = new RooRealVar(m_name+"_"+name+"_tauPos2", "Bck Tau Pos 2", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauNeg               = new RooRealVar(m_name+"_"+name+"_tauNeg",  "Bck Tau Neg",   -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *promptRatio          = new RooRealVar(m_name+"_"+name+"_promptRatio",          "Prompt Ratio",               0., 1.);
        RooRealVar *nonPromptRatio       = new RooRealVar(m_name+"_"+name+"_nonPromptRatio",       "Non Prompt Ratio",           0., 1.);
        RooRealVar *promptNonPromptRatio = new RooRealVar(m_name+"_"+name+"_promptNonPromptRatio", "Prompt to Non Prompt Ratio", 0., 1.);
        RooDecay *decayPos1              = new RooDecay  (m_name+"_"+name+"_decayPos1", "Decay Pos 1", *m_observables->get(TIME), *tauPos1, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayPos2              = new RooDecay  (m_name+"_"+name+"_decayPos2", "Decay Pos 2", *m_observables->get(TIME), *tauPos2, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayNeg               = new RooDecay  (m_name+"_"+name+"_decayNeg",  "Decay Neg",   *m_observables->get(TIME), *tauNeg,  *m_timeResolutionPDF, RooDecay::Flipped); // DoubleSided x Flipped
        RooAddPdf *prompt                = new RooAddPdf (m_name+"_"+name+"_prompt", "Prompt Background", RooArgList(*m_timeResolutionPDF, *decayNeg), *promptRatio);
        RooAddPdf *nonPrompt             = new RooAddPdf (m_name+"_"+name+"_nonPrompt", "Non Prompt Background", RooArgList(*decayPos1, *decayPos2), *nonPromptRatio);
        m_timeBackgroundPDF              = new RooAddPdf (m_name+"_"+name+"_total", "Lifetime Background (Prompt+NegativeExpo and Two PositiveExpo)", RooArgList(*prompt, *nonPrompt), *promptNonPromptRatio);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeBackground[0] = name;
    m_timeBackground[1] = pdf;
}

void ModelPDF::setMassSignalErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *dummy        = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_massSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Signal Error 1Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        RooRealVar *dummy        = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_massSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Signal Error 2Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3        = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3        = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3        = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_massSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Signal Error 3Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massSignalError[0] = name;
    m_massSignalError[1] = pdf;
}

void ModelPDF::setMassBackgroundErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1            = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1            = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1            = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *dummy         = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_massBackgroundErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Background Error 1Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        RooRealVar *dummy        = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_massBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Mass Background Error 2Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3           = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3           = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3           = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2    = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_massBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Mass Background Error 3Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massBackgroundError[0] = name;
    m_massBackgroundError[1] = pdf;
}

void ModelPDF::setTimeSignalErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *dummy     = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_timeSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Lifetime Signal Error 1Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        RooRealVar *dummy     = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_timeSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Lifetime Signal Error 2Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3        = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3        = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3        = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_timeSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Lifetime Signal Error 3Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeSignalError[0] = name;
    m_timeSignalError[1] = pdf;
}

void ModelPDF::setTimeBackgroundErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *dummy        = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_timeBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Lifetime Background Error 1Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        RooRealVar *dummy        = new RooRealVar(m_name+"_"+name+"_dummy", "dummy", 1.);
        m_timeBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Lifetime Background Error 2Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *dummy, *dummy, *dummy, *dummy);
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3           = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3           = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3           = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2    = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_timeBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Lifetime Background Error 3Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeBackgroundError[0] = name;
    m_timeBackgroundError[1] = pdf;
}

void ModelPDF::createModel(void) {
    cout << "INFO: Creating model \"" << m_name << "\"" << endl;

    RooRealVar *nEvents         = new RooRealVar(m_name+"_nEvents",   "Number of Events",     m_dataset->numEntries());
    RooRealVar *nSig            = new RooRealVar(m_name+"_nSig",      "Number of Signal",     0, m_dataset->numEntries());
    RooRealVar *nBck            = new RooRealVar(m_name+"_nBck",      "Number of Background", 0, m_dataset->numEntries());
    RooRealVar *sigFrac         = new RooRealVar(m_name+"_sigFrac",   "Signal Fraction",      0., 1.);
    RooRealVar *fitStatus       = new RooRealVar(m_name+"_fitStatus", "Fit Status",           -1);
    RooRealVar *fitEDM          = new RooRealVar(m_name+"_fitEDM",    "Fit EDM",              0.);
    RooRealVar *fitNLL          = new RooRealVar(m_name+"_fitNLL",    "Fit NLL",              0.);

    if ( m_totalPDF != nullptr ) {
        cout << "ERROR: totalPDF problem!" << endl;
        gApplication->Terminate();
    }

    if ( m_massResolutionPDF != nullptr && m_massSignalCorePDF != nullptr ) {
        m_observables->get(MASS)->setBins(10000, "fft"); // "fft" -> "cache" ?
//         if ( m_xMax > m_xMin ) {
//             cout << "INFO: Setting range" << endl;
//             m_observables->get(MASS)->setMin("fft", m_xMin);
//             m_observables->get(MASS)->setMax("fft", m_xMax);
// //             m_observables->get(MASS)->setMin("fft", 5500.);
// //             m_observables->get(MASS)->setMax("fft", 5750.);
//         }
        m_massSignalPDF = new RooFFTConvPdf("convMassSignal", "Convoluted Mass Signal PDF", *m_observables->get(MASS), *m_massSignalCorePDF, *m_massResolutionPDF);
    } else {
        m_massSignalPDF = m_massSignalCorePDF;
    }

    if ( m_massSignalPDF != nullptr && m_massBackgroundPDF != nullptr ) {
        if ( m_massSignalErrorPDF != nullptr && m_massBackgroundErrorPDF == nullptr ) {
            cout << "ERROR: Punzi for mass background is missing!" << endl;
            gApplication->Terminate();
        }
        if ( m_massSignalErrorPDF == nullptr && m_massBackgroundErrorPDF != nullptr ) {
            cout << "ERROR: Punzi for mass signal is missing!" << endl;
            gApplication->Terminate();
        }
    }

    if ( m_timeSignalPDF != nullptr && m_timeBackgroundPDF != nullptr ) {
        if ( m_timeSignalErrorPDF != nullptr && m_timeBackgroundErrorPDF == nullptr ) {
            cout << "ERROR: Punzi for time background is missing!" << endl;
            gApplication->Terminate();
        }
        if ( m_timeSignalErrorPDF == nullptr && m_timeBackgroundErrorPDF != nullptr ) {
            cout << "ERROR: Punzi for time signal is missing!" << endl;
            gApplication->Terminate();
        }
    }

    if ( m_massSignalPDF != nullptr && m_massSignalErrorPDF != nullptr ) {
        m_totalMassSignalPDF = new RooProdPdf("condMassSignal", "F(x|dx).G(dx)", *m_massSignalErrorPDF, RooFit::Conditional(*m_massSignalPDF, *m_observables->get(MASS)));
        m_conditionalObservables.add(*m_observables->get(MASSERROR)); // HACK ?
        m_useProjWDataMass = false;
    } else if ( m_massSignalPDF != nullptr ) {
        m_totalMassSignalPDF = m_massSignalPDF;
    } else if ( m_massSignalErrorPDF != nullptr ) {
        m_totalMassSignalPDF = m_massSignalErrorPDF;
    }

    if ( m_timeSignalPDF != nullptr && m_timeSignalErrorPDF != nullptr ) {
        m_totalTimeSignalPDF = new RooProdPdf("condTimeSignal", "F(x|dx).G(dx)", *m_timeSignalErrorPDF, RooFit::Conditional(*m_timeSignalPDF, *m_observables->get(TIME)));
        m_conditionalObservables.add(*m_observables->get(TIMEERROR)); // HACK ?
        m_useProjWDataTime = false;
    } else if ( m_timeSignalPDF != nullptr ) {
        m_totalTimeSignalPDF = m_timeSignalPDF;
    } else if ( m_timeSignalErrorPDF != nullptr ) {
        m_totalTimeSignalPDF = m_timeSignalErrorPDF;
    }

    if ( m_totalMassSignalPDF != nullptr && m_totalTimeSignalPDF != nullptr ) {
        m_signalPDF = new RooProdPdf("signalModel", "Signal Model", RooArgSet(*m_totalMassSignalPDF, *m_totalTimeSignalPDF));
    } else if ( m_totalMassSignalPDF != nullptr ) {
        m_signalPDF = m_totalMassSignalPDF;
    } else if ( m_totalTimeSignalPDF != nullptr ) {
        m_signalPDF = m_totalTimeSignalPDF;
    }

    if ( m_massBackgroundPDF != nullptr && m_massBackgroundErrorPDF != nullptr ) {
        m_totalMassBackgroundPDF = new RooProdPdf("condMassBackground", "F(x|dx).G(dx)", *m_massBackgroundErrorPDF, RooFit::Conditional(*m_massBackgroundPDF, *m_observables->get(MASS)));
        m_conditionalObservables.add(*m_observables->get(MASSERROR)); // HACK ?
        m_useProjWDataMass = false;
    } else if ( m_massBackgroundPDF != nullptr ) {
        m_totalMassBackgroundPDF = m_massBackgroundPDF;
    } else if ( m_massBackgroundErrorPDF != nullptr ) {
        m_totalMassBackgroundPDF = m_massBackgroundErrorPDF;
    }

    if ( m_timeBackgroundPDF != nullptr && m_timeBackgroundErrorPDF != nullptr ) {
        m_totalTimeBackgroundPDF = new RooProdPdf("condTimeBackground", "F(x|dx).G(dx)", *m_timeBackgroundErrorPDF, RooFit::Conditional(*m_timeBackgroundPDF, *m_observables->get(TIME)));
        m_conditionalObservables.add(*m_observables->get(TIMEERROR)); // HACK ?
        m_useProjWDataTime = false;
    } else if ( m_timeBackgroundPDF != nullptr ) {
        m_totalTimeBackgroundPDF = m_timeBackgroundPDF;
    } else if ( m_timeBackgroundErrorPDF != nullptr ) {
        m_totalTimeBackgroundPDF = m_timeBackgroundErrorPDF;
    }

    if ( m_totalMassBackgroundPDF != nullptr && m_totalTimeBackgroundPDF != nullptr ) {
        m_backgroundPDF = new RooProdPdf("backgroundModel", "Background Model", RooArgSet(*m_totalMassBackgroundPDF, *m_totalTimeBackgroundPDF));
    } else if ( m_totalMassBackgroundPDF != nullptr ) {
        m_backgroundPDF = m_totalMassBackgroundPDF;
    } else if ( m_totalTimeBackgroundPDF != nullptr ) {
        m_backgroundPDF = m_totalTimeBackgroundPDF;
    }

    if ( m_signalPDF != nullptr && m_backgroundPDF != nullptr ) {
        if ( m_extendedFit ) {
            m_totalPDF = new RooAddPdf("totalModel", "", RooArgList(*m_signalPDF, *m_backgroundPDF), RooArgList(*nSig, *nBck));
        } else {
            m_totalPDF = new RooAddPdf("totalModel", "", RooArgList(*m_signalPDF, *m_backgroundPDF), *sigFrac);
        }
    } else if ( m_signalPDF != nullptr ) {
        m_totalPDF = m_signalPDF;
    } else if ( m_backgroundPDF != nullptr ) {
        m_totalPDF = m_backgroundPDF;
    } else {
        cout << "ERROR: totalPDF problem!" << endl;
        gApplication->Terminate();
    }

    m_pars = (RooArgSet*)m_totalPDF->getParameters(*m_dataset);
    m_pars->add(*nEvents);
    m_pars->add(*fitStatus);
    m_pars->add(*fitEDM);
    m_pars->add(*fitNLL);
}

RooAbsPdf* ModelPDF::getTotalMassSignalPDF(void) {
    if ( m_totalPDF == nullptr ) createModel();
    return m_totalMassSignalPDF;
}

RooArgSet* ModelPDF::getPars(void) {
    if ( m_totalPDF == nullptr ) createModel();
    if ( m_finalPars != nullptr ) return m_finalPars;
    else return m_pars;
}

RooRealVar* ModelPDF::getPars(const TString &varName) {
    if ( m_totalPDF == nullptr ) createModel();
    if ( m_finalPars != nullptr ) return (RooRealVar*)m_finalPars->find(m_name+"_"+varName);
    else return (RooRealVar*)m_pars->find(m_name+"_"+varName);
}

RooRealVar* ModelPDF::getInitialPars(const TString &varName) {
    RooRealVar *par = nullptr;
    if ( m_totalPDF == nullptr ) createModel();
    if ( m_initialPars != nullptr ) {
        par = (RooRealVar*)m_initialPars->find(m_name+"_"+varName);
    } else {
        cout << "ERROR: getInitialPars problem!" << endl;
        gApplication->Terminate();
    }
    return par;
}

void ModelPDF::setPars(ModelPDF *model, const Bool_t &fixed) {
    if ( m_totalPDF == nullptr ) createModel();

    TIterator *pars_it = model->getPars()->createIterator();
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
        TString name = p->GetName();
        name.Remove(0, model->getName().Length()+1);
        RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+name);
        if ( varPtr ) {
            varPtr->setVal(p->getVal());
            if ( fixed ) varPtr->setConstant(kTRUE);
            else         varPtr->setConstant(kFALSE);
        }
    }
}

void ModelPDF::setPars(const TString &varName, const Double_t &val, const Bool_t &fixed) {
    if ( m_totalPDF == nullptr ) createModel();
    RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+varName);
    if ( varPtr ) {
        varPtr->setVal(val);
//         varPtr->setError(1e-6);
        if ( fixed ) varPtr->setConstant(kTRUE);
        else         varPtr->setConstant(kFALSE);
    }
}

void ModelPDF::setPars(const TString &varName, const Double_t &val, const Double_t &x1, const Double_t &x2) {
    if ( m_totalPDF == nullptr ) createModel();
    RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+varName);
    if ( varPtr ) {
        varPtr->setVal(val);
        varPtr->setRange(x1, x2);
    }
}

void ModelPDF::drawPlot(const Observable &var, const TString &title) {
    drawPlot(var, m_observables->get(var)->getMin(), m_observables->get(var)->getMax(), 100, title);
}

void ModelPDF::drawPlot(const Observable &var, const Double_t &xMin, const Double_t &xMax, const TString &title) {
    drawPlot(var, xMin, xMax, 100, title);
}

void ModelPDF::drawPlot(const Observable &var, const Int_t &nBins, const TString &title) {
    drawPlot(var, m_observables->get(var)->getMin(), m_observables->get(var)->getMax(), nBins, title);
}

void ModelPDF::drawPlot(const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title) {
    TPad *pad = (TPad*)gPad;
    gStyle->SetLineScalePS();
    RooPlot *frame = m_observables->get(var)->frame(xMin, xMax, nBins);
    std::vector<TString> titles = splitString(title, ":");
    if ( titles.size() > 0 ) frame->SetTitle(titles.at(0).Data());
    if ( titles.size() > 1 ) frame->GetXaxis()->SetTitle(titles.at(1).Data());
    if ( titles.size() > 2 ) frame->GetYaxis()->SetTitle(titles.at(2).Data());
    frame->GetYaxis()->SetTitleOffset(1.5);

    TLegend *legend = new TLegend(0.69, 0.55, 0.89, 0.40); // 0.69, 0.85, 0.89, 0.70
//     TLegend *legend = new TLegend(0.69, 0.81, 0.89, 0.61); // Bs
    legend->SetFillColor(kWhite);
    legend->SetLineColor(kWhite);
    legend->SetTextFont(42);
    legend->SetTextSize(0.045);
    legend->SetShadowColor(kWhite);

    RooAbsPdf *sigPDF = nullptr;
    RooAbsPdf *bckPDF = nullptr;

    switch( var ) {
        case MASS :
            bckPDF = m_totalMassBackgroundPDF;
            sigPDF = m_totalMassSignalPDF;
            break;
        case MASSERROR :
            bckPDF = m_massBackgroundErrorPDF;
            sigPDF = m_massSignalErrorPDF;
            break;
        case TIME :
            bckPDF = m_totalTimeBackgroundPDF;
            sigPDF = m_totalTimeSignalPDF;
            break;
        case TIMEERROR :
            bckPDF = m_timeBackgroundErrorPDF;
            sigPDF = m_timeSignalErrorPDF;
            break;
        case PT :
            break;
    }

    m_dataset->plotOn(frame, RooFit::DataError(RooAbsData::SumW2), RooFit::Name("data"));
    legend->AddEntry(frame->findObject("data"), "Data", "p");
    if ( m_finalPars != nullptr ) {
        if ( bckPDF != nullptr || sigPDF != nullptr ) {
            Bool_t useProjWData = ( m_useProjWDataMass || m_useProjWDataTime );
            if ( useProjWData ) cout << "INFO: Using ProjWData" << endl;
            if ( sigPDF != nullptr ) {
                if ( useProjWData ) {
                    // HACK to make ProjWData plotting the PDF with correct normalization: RooFit::Normalization(1./m_dataset->numEntries(),RooAbsReal::Relative)
                    m_totalPDF->plotOn(frame, RooFit::Components(sigPDF->GetName()), RooFit::LineColor(kGreen), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("sigPDF"), RooFit::ProjWData(*m_dataset), RooFit::Normalization(1./m_dataset->numEntries(),RooAbsReal::Relative));
                } else {
                    m_totalPDF->plotOn(frame, RooFit::Components(sigPDF->GetName()), RooFit::LineColor(kGreen), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("sigPDF"));
                }
                legend->AddEntry(frame->findObject("sigPDF"), "Signal", "l");
            }
            if ( bckPDF != nullptr ) {
                if ( useProjWData ) {
                    m_totalPDF->plotOn(frame, RooFit::Components(bckPDF->GetName()), RooFit::LineColor(kBlue), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("bckPDF"), RooFit::ProjWData(*m_dataset), RooFit::Normalization(1./m_dataset->numEntries(),RooAbsReal::Relative));
                } else {
                    m_totalPDF->plotOn(frame, RooFit::Components(bckPDF->GetName()), RooFit::LineColor(kBlue), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("bckPDF"));
                }
                legend->AddEntry(frame->findObject("bckPDF"), "Background", "l");
            }
            if ( bckPDF != nullptr && sigPDF != nullptr ) {
                if ( useProjWData ) {
                    m_totalPDF->plotOn(frame, RooFit::LineColor(kRed), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("totPDF"), RooFit::ProjWData(*m_dataset), RooFit::Normalization(1./m_dataset->numEntries(),RooAbsReal::Relative));
                } else {
                    m_totalPDF->plotOn(frame, RooFit::LineColor(kRed), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("totPDF"));
                }
                legend->AddEntry(frame->findObject("totPDF"), "Fit", "l");
            }
//             m_totalPDF->paramOn(frame, RooFit::Label("Fit result"), RooFit::Format("NEU", RooFit::AutoPrecision(1)));
        }
    }

    RooHist *pullHist = frame->pullHist();

    pad->Divide(1,2);
    TPad* pad1 = new TPad("pad1", "pad1", 0., 0.25, 1., 1.);
    TPad* pad2 = new TPad("pad2", "pad2", 0., 0., 1., 0.25);
    pad1->Draw();
    pad2->Draw();

    pad1->cd();
    if ( var == TIME ) {
        pad1->SetLogy();
        pad1->Update();
    }
    pad1->SetTopMargin(0.1);
    pad2->SetTopMargin(0.1);
    pad1->SetRightMargin(0.09);
    pad2->SetRightMargin(0.09);
    pad1->SetBottomMargin(0.13);
    pad2->SetBottomMargin(0.21);
    pad1->SetLeftMargin(0.15);
    pad2->SetLeftMargin(0.15);
    frame->GetYaxis()->SetTitleOffset(1.1); // AS
    frame->GetXaxis()->SetTitleOffset(1.1); // AS
    frame->Draw();
    legend->Draw();
    pad1->Update();

    pad2->cd();
    pad2->DrawFrame(xMin, -5.0, xMax, 5.0);
    if ( m_finalPars != nullptr ) {
        pullHist->GetXaxis()->SetRange(xMin, xMax);
        pullHist->Draw();
    }
    pad2->Update();
    gPad = pad;
}

void ModelPDF::fit(void) {
    if ( m_totalPDF == nullptr ) createModel();

    cout << "INFO: Start of the fit \"" << m_name << "\", using " << m_nCPU << " cores" << endl;
    cout << "\n  ======================================================\n" << endl;
    cout << "  "; m_dataset->Print();
    cout << "  "; m_pars->Print();
    cout << "  "; m_totalPDF->Print();
    cout << "\n  ======================================================\n" << endl;

    m_initialPars = (RooArgSet*)m_pars->snapshot();

    RooLinkedList fitOptions;
    fitOptions.Add(RooFit::Save().Clone());
    fitOptions.Add(RooFit::NumCPU(m_nCPU).Clone());
    fitOptions.Add(RooFit::PrintEvalErrors(-1).Clone());
    fitOptions.Add(RooFit::Warnings(kFALSE).Clone());
    fitOptions.Add(RooFit::Verbose(kFALSE).Clone());
    fitOptions.Add(RooFit::PrintLevel(1).Clone());
    fitOptions.Add(RooFit::SumW2Error(kTRUE).Clone());
    fitOptions.Add(RooFit::InitialHesse(kTRUE).Clone());
    if ( m_conditionalObservables.getSize() > 0 ) fitOptions.Add(RooFit::ConditionalObservables(m_conditionalObservables).Clone()); // HACK ?
    if ( m_xMax > m_xMin ) fitOptions.Add(RooFit::Range(m_xMin, m_xMax).Clone());

    m_fitResult = m_totalPDF->fitTo(*m_dataset, fitOptions);
    cout << "INFO: End of the fit \"" << m_name << "\"" << endl;
    m_finalPars = (RooArgSet*)m_pars->snapshot();

    m_pars = m_finalPars;
    setPars("fitStatus", m_fitResult->status(),   true);
    setPars("fitEDM",    m_fitResult->edm(),      true);
    setPars("fitNLL",    m_fitResult->minNll(),   true);
}

void ModelPDF::saveResults(const TString &name) {
    FILE *file;
    file = fopen(name,"w");
    if ( file != NULL ) {
        if ( m_massSignalPDF          != nullptr ) fprintf(file, "Model:%s\n", m_massSignalPDF->GetName());
        if ( m_massSignalErrorPDF     != nullptr ) fprintf(file, "Model:%s\n", m_massSignalErrorPDF->GetName());
        if ( m_massBackgroundPDF      != nullptr ) fprintf(file, "Model:%s\n", m_massBackgroundPDF->GetName());
        if ( m_massBackgroundErrorPDF != nullptr ) fprintf(file, "Model:%s\n", m_massBackgroundErrorPDF->GetName());
        if ( m_timeSignalPDF          != nullptr ) fprintf(file, "Model:%s\n", m_timeSignalPDF->GetName());
        if ( m_timeSignalErrorPDF     != nullptr ) fprintf(file, "Model:%s\n", m_timeSignalErrorPDF->GetName());
        if ( m_timeBackgroundPDF      != nullptr ) fprintf(file, "Model:%s\n", m_timeBackgroundPDF->GetName());
        if ( m_timeBackgroundErrorPDF != nullptr ) fprintf(file, "Model:%s\n", m_timeBackgroundErrorPDF->GetName());
        if ( m_timeResolutionPDF      != nullptr ) fprintf(file, "Model:%s\n", m_timeResolutionPDF->GetName());

        TIterator *pars_it = m_finalPars->createIterator();
        for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
            fprintf(file, "Pars:%s:%.9e:%.9e\n", p->GetName(), p->getVal(), p->getError());
        }

        fclose(file);
    } else {
        cout << "ERROR: Can't save results into the file (" << name << ")" << endl;
    }
}

void ModelPDF::readResults(const TString &name) {
    cout << "INFO: Reading parameters from the file \"" << name << endl;
    if ( m_totalPDF == nullptr ) createModel();
    cout << "\n  ======================================================\n" << endl;
    cout << "  "; m_pars->Print();
    cout << "  "; m_totalPDF->Print();
    cout << "\n  ======================================================\n" << endl;

    ifstream file(name);
    string line;

    while ( getline(file, line) ) {
        stringstream lineStream(line);
        string data;
        std::vector<string> dataVector;

        while ( getline(lineStream, data, ':') ) dataVector.push_back(data);
        RooRealVar *varPtr = (RooRealVar*)m_pars->find((TString)dataVector[1]);
        if ( varPtr ) {
            varPtr->setVal(stod(dataVector[2]));
            varPtr->setConstant(kTRUE);
            cout << "  Setting " << varPtr->GetName() << " to " << varPtr->getVal() << endl;
        }
    }
    cout << "\n  ======================================================\n" << endl;
}

void ModelPDF::printModel(const TString &options) {
    cout << "INFO: Printing model" << endl;
    cout << "\n  ======================================================\n" << endl;
    cout << "  Model Name: " << m_name << endl;
    if ( m_massSignalCorePDF      != nullptr ) cout << "    " << m_massSignalPDF->GetTitle() << endl;
    if ( m_massSignalErrorPDF     != nullptr ) cout << "    " << m_massSignalErrorPDF->GetTitle() << endl;
    if ( m_massBackgroundPDF      != nullptr ) cout << "    " << m_massBackgroundPDF->GetTitle() << endl;
    if ( m_massBackgroundErrorPDF != nullptr ) cout << "    " << m_massBackgroundErrorPDF->GetTitle() << endl;
    if ( m_massResolutionPDF      != nullptr ) cout << "    " << m_massResolutionPDF->GetTitle() << endl;
    if ( m_timeSignalPDF          != nullptr ) cout << "    " << m_timeSignalPDF->GetTitle() << endl;
    if ( m_timeSignalErrorPDF     != nullptr ) cout << "    " << m_timeSignalErrorPDF->GetTitle() << endl;
    if ( m_timeBackgroundPDF      != nullptr ) cout << "    " << m_timeBackgroundPDF->GetTitle() << endl;
    if ( m_timeBackgroundErrorPDF != nullptr ) cout << "    " << m_timeBackgroundErrorPDF->GetTitle() << endl;
    if ( m_timeResolutionPDF      != nullptr ) cout << "    " << m_timeResolutionPDF->GetTitle() << endl;
    cout << "\n  ======================================================\n" << endl;
}

void ModelPDF::printResults(const TString &options) {
    if ( m_finalPars == nullptr ) return;
    Bool_t printConst = options.Contains("const");
    cout << "INFO: Printing results" << endl;
    cout << "\n  ======================================================\n" << endl;
    cout << "  Model   = " << m_name << endl;
    cout << "  Status  = " << getPars("fitStatus")->getVal() << endl;
    cout << "  EDM     = " << getPars("fitEDM")->getVal() << endl;
    cout << "  NLL     = " << getPars("fitNLL")->getVal() << endl;
    cout << "  #Events = " << getPars("nEvents")->getVal() << endl;
    if ( options.Contains("selection") ) cout << "  Cut     = " << m_dataset->GetTitle() << endl << endl;
    cout << "\n  ------------------------------------------------------\n" << endl;

    TIterator *pars_it = m_finalPars->createIterator();
    Int_t strSize = 0;
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
        TString name = p->GetName();
        Int_t tmpStrSize = name.Length() - ( m_name.Length() + 1 );
        if ( tmpStrSize > strSize ) strSize = tmpStrSize;
    }
    pars_it->Reset();
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
        TString name = p->GetName();
        name.Remove(0, m_name.Length()+1);
        if ( p->getError() != 0. ) {
            TString str = d2string(p->getError(), "%.3g");
            TPRegexp("(\\d{1,}).(\\d{0,})").Substitute(str, "$2");
            TString format = "%# .";
            format += str.Length();
            format += "f";
            printf("  %-*s = %-*s +/-%-*s  [ %#.3f | ", strSize, name.Data(), 9, d2string(p->getVal(), format.Data()).Data(), 9, d2string(p->getError(), format.Data()).Data(), getInitialPars(name)->getVal());
            ( p->getMin() == -RooNumber::infinity() ? printf("-inf.") : printf("%#.3f", p->getMin()) );
            printf(" -> ");
            ( p->getMax() == RooNumber::infinity() ? printf("inf.") : printf("%#.3f", p->getMax()) );
            printf(" ]\n");
        } else {
            if ( printConst ) printf("  %-*s = %-*s  [ const. ]\n", strSize, name.Data(), 22, d2string(p->getVal(), "%# .6g").Data());
        }
    }
    cout << "\n  ======================================================\n" << endl;
}
// TMP!!!
void ModelPDF::pars(const TString &pName) {
        RooRealVar *p = getPars( pName );
        if ( p->getError() != 0. ) {
            TString str = d2string(p->getError(), "%.3g");
            TPRegexp("(\\d{1,}).(\\d{0,})").Substitute(str, "$2");
            TString format = "%# .";
            format += str.Length();
            format += "f";
            printf("%-*s +/-%-*s", 9, d2string(p->getVal(), format.Data()).Data(), 9, d2string(p->getError(), format.Data()).Data());
        } else {
            printf("%-*s", 9, d2string(p->getVal(), "%# .6g").Data());
        }
    }

void ModelPDF::printTWiki(const TString &selection) {
//     system("iLumiCalc.exe --lumitag=OflLumi-13TeV-005 --livetrigger=L1_EM12 --trigger=None --xml=data16_13TeV.periodAllYear_DetStatus-v81-pro20-09_DQDefects-00-02-02_PHYS_StandardGRL_All_Good.xml --lar --lartag=LARBadChannelsOflEventVeto-RUN2-UPD4-04 2>&1 | tail -n 15 | grep \"Total IntL recorded\" | awk '{ print \"TWIKI: | "+selection+" | \" $9 / 1e6 }'");
    cout << "TWIKI: | "<< selection << " | IntL ";
    cout << " | ";
    cout << m_dataset->mean(*m_observables->get(PT)) / 1000;
    cout << " | ";
    cout << getPars("nEvents")->getVal();
    cout << " | ";
    pars("sigFrac");
    cout << " | ";
    cout << (Int_t)(getPars("nEvents")->getVal() * getPars("sigFrac")->getVal());
    cout << " | BsSigPerInvPb | ";
    pars("timeSig_tau");
    cout << " | ";
    pars("massSig_mean");
    cout << " |" << endl;
}

RooRealVar* ModelPDF::getNSig() {
     RooRealVar *result = new RooRealVar("_nSigResult", "Number of signal events in range", -RooNumber::infinity(), RooNumber::infinity());
         if(m_extendedFit){
        result->setVal( getPars("nSig")->getVal());
        result->setError( getPars("nSig")->getError());
    }else{
        result->setVal( getPars("nEvents")->getVal() * getPars("sigFrac")->getVal());
        result->setError( getPars("nEvents")->getVal() * getPars("sigFrac")->getError());
    }
     return result;
}

RooRealVar* ModelPDF::getNSigInRange(const Double_t &x1, const Double_t &x2) {
    m_observables->get(MASS)->setRange("_nSigRange", x1, x2);
    RooAbsReal *integral = m_massSignalPDF->createIntegral(* m_observables->get(MASS), RooFit::NormSet(* m_observables->get(MASS)), RooFit::Range("_nSigRange"));
    RooRealVar *result = new RooRealVar("_nSigInRange", "Number of signal events in range", -RooNumber::infinity(), RooNumber::infinity());
    if(m_extendedFit){
        result->setVal(integral->getVal() * getPars("nSig")->getVal());
        result->setError(integral->getVal() * getPars("nSig")->getError());
    }else{
        result->setVal(integral->getVal() * getPars("nEvents")->getVal() * getPars("sigFrac")->getVal());
        result->setError(integral->getVal() * getPars("nEvents")->getVal() * getPars("sigFrac")->getError());
    }
    return result;
}

RooRealVar* ModelPDF::getNBkgInRange(const Double_t &x1, const Double_t &x2) {
    m_observables->get(MASS)->setRange("_nBckRange", x1, x2);
    RooAbsReal *integral = m_massBackgroundPDF->createIntegral(* m_observables->get(MASS), RooFit::NormSet(* m_observables->get(MASS)), RooFit::Range("_nBckRange"));
    RooRealVar *result = new RooRealVar("_nBkgInRange", "Number of background events in range", -RooNumber::infinity(), RooNumber::infinity());
    if(m_extendedFit){
        result->setVal(integral->getVal() * getPars("nBck")->getVal());
        result->setError(integral->getVal() * getPars("nBck")->getError());
    }else{
        result->setVal(integral->getVal() * getPars("nEvents")->getVal() * (1 - getPars("sigFrac")->getVal()));
        result->setError(integral->getVal() * getPars("nEvents")->getVal() * getPars("sigFrac")->getError());
    }
    return result;
}