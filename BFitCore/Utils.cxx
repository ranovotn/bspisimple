#include "Utils.h"

Bool_t rootFileExists(TString fileName) {
    Bool_t fileExists;
    Int_t oldErrorIgnoreLevel = gErrorIgnoreLevel;
    gErrorIgnoreLevel = kBreak;
    TFile f(fileName, "READ");
    gErrorIgnoreLevel = oldErrorIgnoreLevel;
    if ( f.IsZombie() ) fileExists = kFALSE;
    else                fileExists = kTRUE;
    f.Close();
    return fileExists;
}

void splitString(const TString &inputString, const Char_t *delimiter, vector<TString> &elements) {
    stringstream ss(inputString.Data());
    string item;
    while (getline(ss, item, *delimiter)) elements.push_back(item);
}

vector<TString> splitString(const TString &inputString, const Char_t *delimiter) {
    vector<TString> elements;
    splitString(inputString, delimiter, elements);
    return elements;
}

TString d2string(Double_t x, const Char_t *format) {
    return Form(format, x);
}

TString printTime(const time_t &t) {
    return Form("%ld:%02ld:%02ld", t / 3600, (t / 60) % 60, t % 60);
}
